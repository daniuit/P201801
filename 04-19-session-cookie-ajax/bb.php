<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-19 10:51:54 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-19 11:00:12
 */
session_start();
session_unset();
session_destroy();
// var_dump($_SESSION['username']);