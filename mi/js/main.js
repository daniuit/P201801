/*
* @Author: zhibinm
* @Date:   2018-03-31 15:25:23
* @Last Modified by:   zhibinm
* @Last Modified time: 2018-04-05 09:33:10
*/
// window.onload= function () {
// 	img_item = document.getElementsByClassName('img_item');
// 	citem = document.getElementsByClassName('citem');
// 	num = 0;
// 	// 循环跑的方法
// 	function run() {
// 		//让所有图片先消失 
// 		for (var i = 0; i < img_item.length; i++) {
// 			img_item[i].style.display ="none";
// 			citem[i].style.background = "#000";
// 		}
		
// 		//其中某一张出现
// 		img_item[num].style.display ="block";
// 		citem[num].style.background = "red";

// 		// 自增到下一张，跑到最后一张重新来
// 		if(num==img_item.length-1){
// 			num = 0;
// 		}else{
// 			num++;
// 		}
// 	}
// 	// 开定时器
// 	time = setInterval(run,1000);

// 	//进入banner区域
// 	banner = document.getElementById('banner');

// 	banner.onmouseover=function () {
// 		clearInterval(time)
// 	}

// 	banner.onmouseout=function () {
// 		time = setInterval(run,1000);
// 	}

// 	// 循环加圆点加事件
// 	for (var k=0; k<citem.length; k++) {
// 		citem[k].index = k;
//  		citem[k].onclick = function () {
// 			num = this.index;
// 			run();
// 		}
// 	}

// 	click_right = document.getElementById('click_right');
// 	// 往右走点击事件 
// 	click_right.onclick = function () {

// 		run();
// 	}

// 	click_left = document.getElementById('click_left');
// 	// 往右走点击事件 
// 	click_left.onclick = function () {
// 		// 最左边最右边有两个临界点
// 		if(num==1){
// 			num = img_item.length-1;
// 		}else if(num==0){
// 			num = img_item.length-2;
// 		}else{
// 			num  = num-2;
// 		}
// 		run();
// 	}
// }
// 


$(function () {
	// 购物车
	$('.car').hover(function() {
		// 先停止本身的动画，再滑动下
		$('.car_show').stop().slideDown(100);
	}, function() {
		// 先停止本身的动画，再滑动上
		$('.car_show').stop().slideUp(300);
	});

	//搜索聚焦
	$('.search .in').focus(function(event) {
		// 让in和btn两个控件的边框颜色发生变化
		$(this).css('border-color', 'red');
		$('.search .btn').css('border-color', 'red');
		// 让下面的盒子text_show出现
		$('.search .text_show').show();
		// 输入框中的关键词隐藏
		$('.keyword a').hide();
	});
	//搜索失焦
	$('.search .in').blur(function(event) {
		// 让in和btn两个控件的边框颜色发生变化
		$(this).css('border-color', '#E0E0E0');
		$('.search .btn').css('border-color', '#E0E0E0');
		// 让下面的盒子text_show隐藏
		$('.search .text_show').hide();
		// 输入框中的关键词出现
		$('.keyword a').show();
	});

	// 菜单消失隐藏
	$('.menu_list .item').hover(function() {
		// 让下面的menu_son盒子出现
		$(this).find('.menu_son').show();
		//让兄弟下面的盒子隐藏
		$(this).siblings().find('.menu_son').hide();
	}, function() {
		// 让下面的menu_son盒子出现
		$(this).find('.menu_son').hide();
	});



	num = 0;

	function run() {
		// 让其中一个的图片淡入，兄弟淡出
		$('.img_item').eq(num).fadeIn().siblings().fadeOut();
		//让其中一个加点背景变红，兄弟背景变黑
		$('.citem').eq(num).css('background', 'red').siblings().css('background', '#000');


		//跑到最后一个，num这为零
		if(num ==$('.img_item').length-1){
			num = 0;
		}else{
			num++;
		}
		
	}

	// 设置定时器
	time = setInterval(run,1000);

	//进入这个区域停定时器，出来这个区域开定时器
	$('#banner').hover(function() {
		clearInterval(time);
	}, function() {
		time = setInterval(run,1000);
	});

	// 给每个加点加点击事件
	$('.citem').click(function(event) {
		num = $(this).index();
		run();
	});

	//点右，跟定时方向一样
	$('#click_right').click(function(event) {
		run();
	});
	//点左，跟定时器方向不一样，有两个临界点
	$('#click_left').click(function(event) {
		// 当num等于1于，把num变成最后一个
		// 当num等于0时，把num变成倒数第二个
		if(num==1){
			num = $('.img_item').length-1
		}else if(num==0){
			num = $('.img_item').length-2
		}else{
			num = num-2;
		}

		run();
	});


	// window  document html  body

	$(document).scroll(function(event) {
		height = $(this).scrollTop();
		if(height>600){
			$('.top_box').slideDown();
			$('.to_top').show();
		}else{
			$('.top_box').slideUp();
			$('.to_top').hide();
		}
	});

	$('.to_top').click(function(event) {


		// console.log(document.getElementsByTagName('body'));
		
		// console.log($('html,body'));
		$('html,body').animate({'scrollTop': 0}, 400);

	});

})