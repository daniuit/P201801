/*
* @Author: zhibinm
* @Date:   2018-04-23 09:22:07
* @Last Modified by:   Zhibinm
* @Last Modified time: 2018-04-23 19:00:48
*/
-- 一个库 社区库

create database bbs default character set utf8;

-- 帖子表，回复表，用户表，提问表，收藏表，点赞表


 create table user(
    id int(7) primary key AUTO_INCREMENT COMMENT "主键",
    username char(20) not null COMMENT "用户名",
    password char(32) not null COMMENT "密码",
    face char(100) not null default '' COMMENT "头像",
    age TINYINT not null default 20 COMMENT "年龄",
    sex enum('男','女','保密') default "男",
    city char(10) not null default '' COMMENT "城市",
    login_ip char(15) COMMENT "ip",
    is_admin enum('0','1') default "0" COMMENT "是否管理员",
    ctime int(10) not null default 0 COMMENT "创建时间",
    ltime int(10) not null default 0 COMMENT "登录时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='用户表';

-- 用户主键，用户名，密码，创建时间，登录时间，头像，年龄，性别，城市，登录IP，是否管理员--------用户表

 create table question(
    id int(7) primary key AUTO_INCREMENT COMMENT "主键",
    title char(100) not null COMMENT "标题",
    content text not null COMMENT "内容",
    uid int(10) not null COMMENT "用户id",
    status enum('0','1') default "0" COMMENT "状态（0 未解决 1 已解决）",
    collect int(6) not null default 0 COMMENT "收藏数量",
    zan int(7) not null default 0 COMMENT "点赞数量",
    view_num int(8) not null default 0 COMMENT "浏览量",
    ctime int(10) not null default 0 COMMENT "创建时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='提问表';
-- 提问主键，提问的标题，提问的内容，谁提问的，提问的时间，收藏的数量，点赞的数量，浏览的数量，提问的状态（未解，已解）-----帖子表

create table replay(
    id int(7) primary key AUTO_INCREMENT COMMENT "主键",
    content text not null COMMENT "内容",
    uid int(10) not null COMMENT "用户id",
    qid int(10) not null COMMENT "问题id",
    ctime int(10) not null default 0 COMMENT "创建时间",
    zan int(7) not null default 0 COMMENT "点赞数量",
    status enum('0','1') default "0" COMMENT "状态（0 未采纳 1 已采纳）"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='回复表';
-- 回复主键，回复的内容，谁回复，回复哪个帖子，回复时间，回复点赞数量，状态（最佳状态）------回复表

create table collect(
    id int(7) primary key AUTO_INCREMENT COMMENT "主键",
    uid int(10) not null COMMENT "用户id",
    qid int(10) not null COMMENT "问题id",
    ctime int(10) not null default 0 COMMENT "创建时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='收藏表';
-- 收藏主键，收藏问提，谁收藏的，收藏的时间---------收藏表
create table zan(
    id int(7) primary key AUTO_INCREMENT COMMENT "主键",
    uid int(10) not null COMMENT "用户id",
    rid int(10) not null COMMENT "问题id",
    ctime int(10) not null default 0 COMMENT "创建时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='点赞表';
-- 点赞主键，点赞哪个回复帖子，谁点赞的，点赞的时间----------点赞表


select * from stu;  全表扫描

insert into stu (name,age) value('小胆',18);

update stu set name="ssss" where id>9;

delete from stu where id=90 and name = "小明";


where 筛选符合条件的行

select * from  筛选列（要显示哪些字段）

select name,age from stu where addr="广东" or sex="男";

去重

select DISTINCT  addr from stu;

排序

select * from stu order by scroe desc; 升

select * from stu where sex='男' order by age desc;

取部分数据 

select * from stu limit 3;

select * from stu limit 3,3;

select * from stu where sex='男' order by age desc limit 3;


在什么区间

select * from stu where age between  20 and 30;

select * from stu where id=8 or id=9 or id=3;

select * from stu where id in (8,9,3,6,7,322,2);

搜索区域

select * from stu where name like "%小";

select * from stu where name like "%小%小%";

select * from stu where name like "小%";


select * from stu where class is null;

select * from stu where class is not null;


判断输出 

select name,age,num,if(age>30,'大叔','小伙子') bbb  from stu;

select name,ifnull(class,'xxxx') class from stu;

随机 
select * from stu where sex="男" order by rand() limit 3;

select * from news where cid=2 order by rand() limit 5;




修改表结构

alter table stu rename stu2;

alter table stu change name username char(30) not null default '' comment "用户名";

alter table stu modify scroe int(10);

alter table stu add qq char(11) not null default '1133';

alter table stu drop qq;

alter table stu add qq char(11) not null default '1133' after age;



统计分组

select count(*) cn from stu where age<30;

select sum(scroe) from stu  where sex="男";

select avg(scroe) from stu  where sex="男";

select avg(scroe) from (select * from stu where sex="男") nanbiao;

select max(scroe) from stu;

select * from stu order by scroe asc limit 1;

select * from stu where scroe = (select min(scroe) from stu);

select count(*) cn,addr from stu group by addr;

select avg(scroe),sex,addr from stu group by sex,addr;

select max(age) from stu;

delete from stu where age = 100;
