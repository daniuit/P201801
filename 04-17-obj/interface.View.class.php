<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-17 09:28:07 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-17 09:28:26
 */
interface View{
	public function show();
}