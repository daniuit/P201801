<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-17 08:59:56 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-17 10:09:06
 */

// include './abstract.dongwu.class.php';

// include './Dog.class.php';

// include './Cat.class.php';

// include './interface.Usb.class.php';

// include './interface.View.class.php';

// include './Pc.class.php';
// 当使用到某个类，类没有在之前没有定义，自动执行方法
// 自动加载文件  按需加载 
function __autoload($classname)
{
	$paths = [
		$classname.".class.php",
		"interface.".$classname.".class.php",
		"abstract.".$classname.".class.php"
	];

	foreach ($paths as $path) {
		if(file_exists($path)){
			include $path;
			return ;
		}
	}
}

$dog = new Dog();

var_dump($dog);

// $pc = new Pc();

// $pc->show();

// $pc->game();

// $dog = new Cat();

// $dog->db();

// echo __LINE__;

// define('APP', '学并思');

// echo APP;

// $name = "xbs";

// echo $name;
// echo __LINE__;
// 
// echo __FILE__;
// 
/**
* 
*/
class Test
{
	
	public function run()
	{
		echo "run";
	}

	public function __isset($data)
	{
		var_dump("999");
		var_dump($data);
	}

	public function __unset($data)
	{
		var_dump("999set");
		var_dump($data);
	}

	public function __get($data)
	{
		var_dump("999get");
		var_dump($data);
	}
	public function __set($data,$data1)
	{
		var_dump("999get");
		var_dump($data);
		var_dump($data1);
	}

	public function __call($name,$data)
	{
		var_dump($name);
		var_dump($data);
	}
}

// $obj = new Test();

// $obj->go('567','456');

// $obj->name='90';

// isset($obj->name);
// unset($obj->name);




// $obj->run();
// 在某种条件下，不用调用可以自动执行的方法---魔术方法