<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-17 08:20:54 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-17 08:50:07
 */
/**
* 
*/
class Father
{
	
	public function go()
	{
		echo "gogo";
	}
}
class Test extends Father
{
	public $name = "水果";

	public static $cn = 0;

	public function __construct($data)
	{
		// $this->name = $data;
		self::$cn = $data;
	}
	public function getCn()
	{
		echo self::$cn;
	}
	public function run($data='')
	{
		$this->name = $data;
	}
	
	

}
$obj = new Test("苹果");
$obj->getCn();
$obj1 = new Test("篮子");
$obj->getCn();

// $obj->run('苹果');

// $obj->go();
// 
