<?php

// 定义常量
// 定义框架路径
define("XBS_PATH",'./xbs');
// 定义框架资源路径
define("LIB_PATH",XBS_PATH.'/lib');
// 定义框架公共路径
define("COMMON_PATH",XBS_PATH.'/common');
// 定义框架核心路径
define('CORE_PATH',XBS_PATH.'./core');
// 引入框架全局函数库
require COMMON_PATH.'/functions.php';

// http://127.0.0.1/P201801/xbsphp/index.php?m=index&c=user&a=edit

// 获取路径上的模块-控制器-方法，如果没传走配置的默认值
$module = isset($_GET['m']) ? strtolower($_GET['m']) : config('default_module');
$controller = isset($_GET['c']) ? ucfirst($_GET['c']) : config('default_controller');
$action = isset($_GET['a']) ? strtolower($_GET['a']) : config('default_action');

// var_dump($module ,$controller,$action);
// 定义模块名
define('MODULE_NAME',$module);
// 定义控制器名
define('CONTROLLER_NAME',$controller);
// 定义方法名
define('ACTION_NAME',$action);
// 组模块路径
define('MODULE_PATH',APP_PATH.'/'.MODULE_NAME);
// 组控制器路径
define('CONTROLLER_PATH',MODULE_PATH.'/controller');
// 组模型路径
define('MODEL_PATH',MODULE_PATH.'/model');
// 组视图路径
define('VIEW_PATH',MODULE_PATH.'/view');

// var_dump(MODULE_PATH,CONTROLLER_PATH,MODEL_PATH,VIEW_PATH);

// 自动加载函数，实例化类找不到的时间自动执行
function __autoload($classname)
{   
    // 组可能存在的文件完整路径
    $paths = [
        CONTROLLER_PATH.'/'.$classname.'.php',
        MODEL_PATH.'/'.$classname.'.php',
        CORE_PATH.'/'.$classname.'.php',
        LIB_PATH.'/'.$classname.'.class.php'

    ];
    //循环路径，如果存在就引进来
    foreach ($paths as  $path) {
        if(file_exists($path)){
            require $path;
            return;
        }
    }
}
// 组控制器名
$controllerName = $controller."Controller";
//实例化控制器
$obj = new $controllerName();
//调用控制器里面的方法
$obj->$action();
