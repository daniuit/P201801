<?php

/**
* 控制器基类
*/
class Controller
{
    
    public $data;
    public function display($name='')
    {
        // $xbs = "sdfsdfsdf";

        // $data = "sdfsdfsd";
        extract($this->data);

        if(empty($name)){
            include VIEW_PATH.'/'.CONTROLLER_NAME.'/'.ACTION_NAME.'.html';
        }else{
            include VIEW_PATH.'/'.$name.'.html'; 
        }
        
    }

    public function assign($key,$value)
    {
        $this->data[$key] = $value;
    }
}