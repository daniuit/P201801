<?php

/**
* 
*/
class StuModel extends Model
{
    public $table = "stu";


    public function getMan()
    {
       return $this->where(['sex'=>'男'])->select();
    }

    public function getTopAge()
    {
        return $this->order('age desc')->limit(10)->select();
    }

}