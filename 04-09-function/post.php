<?php
header( 'Content-Type:text/html;charset=utf-8');
/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-09 08:59:25 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-09 09:17:44
 */
$yunsuan = $_POST['yunsuan'];

$num1 = intval($_POST['num1']);

$num2 = intval($_POST['num2']);

if(($num1==0 && $_POST['num1']<>'0')||($num2==0 && $_POST['num2']<>'0')){

	exit("非法字符");
}

switch ($yunsuan) {
	case '+':
		$sum = $num1+$num2;
		break;
	case '-':
		$sum = $num1-$num2;
		break;
	case '*':
		$sum = $num1*$num2;
		break;
	case '/':
		$sum = $num1/$num2;
		break;
}

echo $sum;
