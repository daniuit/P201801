<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-12 15:57:26 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-16 11:02:08
 */
define('CSS', './templete/css');

define('JS', './templete/js');

define('IMAGES', './templete/images');

define("UPLOAD_ROOT",'./Upload');




function notice($url='./index.php',$info="发布成功",$is=true,$time=3)
{
	include './templete/notice.html';
	exit;
}

function getData()
{
	$oldData = file_get_contents('./db/data.txt');

	$oldArr = json_decode($oldData,true);

	return $oldArr;
}

function putData($data)
{
	$str = json_encode($data);

	file_put_contents('./db/data.txt',$str);
}

/*
 * [Ftime 格式化时间]
 * @param [type] $time [时间戳]
 */
function Ftime($time=null)
{
	if($time){

		$diff = time()-$time;

		if($diff<60 && $diff>=0){
			return "刚刚";
		}elseif($diff<3600){
			return floor($diff/60)."分钟之前";
		}elseif($diff<86400){
			return floor($diff/3600)."小时之前";
		}elseif($diff<86400*7){
			return floor($diff/86400)."天之前";
		}else{
			return date("Y-m-d H:i:s",$time);
		}
	}else{
		return date("Y-m-d H:i:s");
	}
}

/**
 * [Fsize 格式化存储单位]
 * @param [type] $size [必传字节数]
 */
function Fsize($size)
{
	if($size<1024){
		return $size."b";
	}elseif($size<pow(1024,2)){
		return round($size/1024,2)."K";
	}elseif($size<pow(1024,3)){
		return round($size/pow(1024,2),2)."M";
	}elseif($size<pow(1024,4)){
		return round($size/pow(1024,3),2)."G";
	}elseif($size<pow(1024,5)){
		return round($size/pow(1024,4),2)."T";
	}
}

function upload($file)
{
	//把文件名转成数据
	$suffix = explode(".", $file['name']);
	//取数组里面最后一项
	$suffix = end($suffix);

	// 设置允许 的类型
	$allowType = ['jpg','png','gif','txt','html','php','zip','rar','jfif'];
	//判断当前 上传的文件是否符合对应的类型,
	if(!in_array($suffix,$allowType)){
		exit("文件类型不允许，允许的类型".implode(',',$allowType));
	}
	//判断文件大小
	if($file['size']>100000){
		exit("文件过大最大是100字节");
	}
	//根据微秒来生成一个唯一文件id
	$name =uniqid().".".$suffix;

	// 目录存在不创建，不存在创建目录
	is_dir(UPLOAD_ROOT) || mkdir(UPLOAD_ROOT);

	// 把文件的移动到想要的目录进行保存
	move_uploaded_file($file['tmp_name'],UPLOAD_ROOT."/".$name);
}

function getSmallImg($file)
{
	$fileName = basename($file);

	$Arr = explode('.',$fileName);

	$suffix = end($Arr);

	if(is_dir($file)){
		return IMAGES."/dir.jpg";
	}

	if(in_array($suffix,['jpg','png','gif','jfif'])){
		return $file;
	}

	switch ($suffix) {
		case 'rar':
			return IMAGES."/rar.jpg";
			break;
		case 'php':
			return IMAGES."/php.jpg";
			break;
		case 'html':
			return IMAGES."/html.jpg";
			break;
		case 'txt':
			return IMAGES."/txt.jpg";
			break;
		case 'zip':
			return IMAGES."/zip.jpg";
			break;
		default:
			return IMAGES."/unkonw.jpg";
			break;
	}
	
}