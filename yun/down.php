<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-16 10:21:58 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-16 10:22:43
 */
include './common.php';

$file = $_GET['file'];

header("Content-type:application/octet-stream");//二进制文件
$fileName = basename($file);//获得文件名
header("Content-Disposition:attachment;filename=$fileName");//下载窗口中显示的文件名
header("Accept-ranges:bytes");//文件尺寸单位  
header("Accept-length:".filesize($file));//文件大小  
readfile($file);//读出文件内容