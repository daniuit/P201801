<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-16 10:54:48 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-16 10:59:40
 */
include "./common.php";

$dir = $_POST['dir'];

$path = UPLOAD_ROOT."/".$dir;

if(is_dir($path)){
	notice('./index.php','目录已经存在',false);
}

$res = mkdir($path);

if($res){
	notice('./index.php','创建成功');
}else{
	notice('./index.php','创建失败，可能没有权限',false);
}