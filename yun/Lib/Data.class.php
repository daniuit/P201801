<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-17 11:21:55 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-17 11:28:37
 */
/**
* 
*/
class Data 
{
	public function ChangeType()
	{
		$data = current($_FILES);

		$timeArr =[];

		foreach ($data['name'] as $k => $v) {

			$file = [];
			$file['name']= $v;
			$file['type'] = $data['type'][$k];
			$file['tmp_name'] = $data['tmp_name'][$k];
			$file['error'] = $data['error'][$k];
			$file['size'] = $data['size'][$k];

			$timeArr[] = $file;
		}

		return $timeArr;
	}
}