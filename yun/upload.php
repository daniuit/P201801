<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-16 09:56:57 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-17 11:18:23
 */



include './common.php';

$data = $_FILES['files'];

foreach ($data['name'] as $k => $v) {

	$file = [];
	$file['name']= $v;
	$file['type'] = $data['type'][$k];
	$file['tmp_name'] = $data['tmp_name'][$k];
	$file['error'] = $data['error'][$k];
	$file['size'] = $data['size'][$k];

	upload($file);
}

notice('./index.php','上传成功');
