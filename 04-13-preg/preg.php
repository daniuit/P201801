<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-13 09:05:45 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-13 11:20:26
 */
// 正则：简单的理解就是对字符串的增，删，改，查

// 应用场景：

// 对字符串验证
// 采集


// 用户名： xuebing87  6-8 字母大小组成
// 邮箱  ：  122@ssdsd.com 
// 手机 ： 18925139194 
// 身份证   


// $str = "sin@sina.com.cn";

// $preg = "/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/";


// ^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}$

// $c = preg_match($preg,$str);

// var_dump($c);
// 

// 处理
$str = "sdf2sdf";

//规则 字符串  定界符
// $preg = "/\+/";

$preg = "/[3-9]/";

// 原子  最小单位 ： 普通原子---特殊原子（加\）--原子表（一定范围的原子）
// 原子组

$c = preg_match($preg,$str);

// var_dump($c);
// 
$str = "www.sina.com,www.163.com";

$preg = "/(sina|163)/"; //选择修改符

$c = preg_replace($preg,'xxxx',$str);

// var_dump($c);

// $c = preg_match($preg,$str);


// var_dump($c);
// 
$str = "www.xuebingsi.com,www.xuebing.com";

$preg = "/(xuebing)(si)/"; //反向引用

$c = preg_replace($preg,'$1xxx$2',$str);

// var_dump($c);
// 量词
// 
$phone = "18925";

$preg = "/\d?/";


$c =preg_match($preg,$phone);

// var_dump($c);

$str = "8888xxx9999";

$preg = "/x{2,4}?/";//正则默认是尽可能的多匹配

$c = preg_replace($preg,'a',$str);

// var_dump($c);



$phone = "15725139194";

// 130 131 132 139 147 151 155 156 157 167 177 189 181 190

// $preg = "/^(13[0-9]|15[179]|17[07]|18[190])\d{8}$/";

$preg = "/^1[3578]\d{9}$/";


$c =preg_match($preg,$phone);

// var_dump($c);


// var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;

// /(^1[3|4|5|7|8]\d{9}$)|(^09\d{8}$)/; 


// ^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$

// 判断 用户名  数字 字母 下划线组成 长度 6-8，不能以数字或者下划线  开头 结尾


$name = "zh0ibin";

$preg = "/^[A-Za-z]\w{4,6}[A-Za-z0-9]$/";

$preg = "/^[A-Z]\w{4,6}[A-Z0-9]$/i";

$c = preg_match($preg,$name);

// var_dump($c);


$name = "B";

$preg = "/b/i";

$c = preg_match($preg,$name);

// var_dump($c);


// 邮箱的正则
// 
// sdfsdf@sdf.com

// 身份证正则
// 1900-2018
// 445281 1986 12 25 1877

// 判断图片正则   345.jpg   435.png  34543.gif
// 


// preg_match();

// preg_replace();
// 

// $str = "jpg|png|gif@bmp";

// $preg = "/(\||@)/";

// $c = preg_split($preg,$str);

// var_dump($c);


$str = "fdsjkweibin78sdfsjdweibin67sdf456";

$preg = "/weibin(\d{2})/";

$c = preg_match_all($preg,$str,$out);

var_dump($c);

var_dump($out);

