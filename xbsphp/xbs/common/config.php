<?php
return [

    'db_type' => 'mysql',

    'db_host'=>'127.0.0.1',

    'db_name'=>'school',

    'db_user'=>'root',

    'db_pass'=>'root',

    'db_charset'=>'utf8',

    'default_module'     =>'index',
    'default_controller' =>'Index',
    'default_action'     =>'index'

];