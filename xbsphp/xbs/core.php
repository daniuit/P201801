<?php

define('XBS_PATH', './xbs');

define('COMMON_PATH', XBS_PATH.'/common');

define('LIB_PATH', XBS_PATH.'/lib');

define('CORE_PATH', XBS_PATH.'/core');

include COMMON_PATH.'/functions.php';

// var_dump(CORE_PATH);
// 获取路径上的模块-控制器-方法，如果没传走配置的默认值
$module = isset($_GET['m']) ? strtolower($_GET['m']) : config('default_module');
$controller = isset($_GET['c']) ? ucfirst($_GET['c']) : config('default_controller');
$action = isset($_GET['a']) ? strtolower($_GET['a']) : config('default_action');

//  $pathInfo= $_SERVER['PATH_INFO'];
//  $arr = explode('/', $pathInfo);
// var_dump($arr);
// exit;
define('MODULE_NAME', $module);

define('CONTROLLER_NAME', $controller);

define('ACTION_NAME', $action);


define('MODULE_PATH',APP_PATH.'/'.MODULE_NAME);


define('CONTROLLER_PATH',MODULE_PATH.'/controller');

define('VIEW_PATH',MODULE_PATH.'/view');

define('MODEL_PATH',MODULE_PATH.'/model');

// var_dump($module,$controller,$action);



function __autoload($classname)
{
    $paths = [
        CONTROLLER_PATH.'/'.$classname.'.php',
        CORE_PATH.'/'.$classname.'.php',
        LIB_PATH .'/'.$classname.'.class.php',
        MODEL_PATH.'/'.$classname.'.php',
    ];


    foreach ($paths as $path) {
        if(file_exists($path)){
            require $path;
            return;
        }
    }
}


$controllerName = $controller."Controller";

$obj = new $controllerName();

$obj->$action();





