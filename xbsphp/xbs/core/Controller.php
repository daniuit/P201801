<?php

/**
* 控制器基类
*/
class Controller
{
    public $data;
    public function display($tempName='')
    {

        // var_dump($this->data);

        extract($this->data);

        if($tempName){
            include VIEW_PATH.'/'.$tempName.'.html';
        }else{
            include VIEW_PATH.'/'.CONTROLLER_NAME.'/'.ACTION_NAME.'.html';
        }
        
    }

    public function assign($k,$v)
    {
        $this->data[$k] = $v;
    }
}