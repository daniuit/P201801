<?php

namespace app\admin\model;

use think\Model;
use think\Db;

/**
* 
*/
class Category extends Model
{
	public $cates;
	public $res = [];
	public function getTree()
	{
		$this->cates = Db::table('fly_category')->order('list_order desc')->select();

		$this->changeData();

		return $this->res;
	}

	public function changeData($fid=0,$level=0)
	{
		// $is_son=false;

		foreach ($this->cates as $cate) {

			if($cate['fid']==$fid){

				// $is_son = true;

				$cate['margin'] = str_repeat('&nbsp;',$level*8);

				$cate['is_son'] = $this->getSon($cate['cid']);

				$this->res[] = $cate;

				// $index = count($this->res)-1;

				$dd =  $this->changeData($cate['cid'],$level+1);

				// $this->res[$index]['is_son'] = $dd;
			}
		}

		// return $is_son;

	}

	public function getSon($cid)
	{
		$res = false;
		foreach ($this->cates as $cate) {
			if($cate['fid']==$cid){
				$res = true;
			}
		}
		return  $res;
	}
}