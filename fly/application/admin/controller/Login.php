<?php
namespace app\admin\controller;

use think\Controller;
// use app\index\model\Vercode;
// use app\index\model\User;
use think\Loader;
// use phpmailer\Mail;
// use alimsg\Phone;
// use Wuliu\Wuliu;
// use Wuliu\Wl;
use think\captcha\Captcha;
use app\common\controller\Common;

class Login extends Common
{
    public function index()
    {
        return $this->fetch();
    }

    public function code()
    {
        $config =    [
            // 验证码字体大小
            'fontSize'    =>    30,    
            // 验证码位数
            'length'      =>    3,   
            // 关闭验证码杂点
            'useNoise'    =>    true, 
        ];
        $captcha = new Captcha($config);
        return $captcha->entry();
    }

    public function checkdata()
    {
        $data = input('post.');

        $validate = Loader::validate('Login');

        if(!$validate->check($data)){
            $this->error($validate->getError());
        }

        $this->success('登录成功');

    }



}