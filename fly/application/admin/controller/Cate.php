<?php
namespace app\admin\controller;

use think\Controller;
use app\common\controller\Common;
use app\admin\model\Category;

class Cate extends Common
{
    public function index()
    {
    	$data = (new Category)->getTree();

    	// var_dump($data);

    	$this->assign('cates',$data);

        return $this->fetch();
    }

    public function welcome()
    {
        return $this->fetch();
    }
}
