<?php
namespace app\admin\controller;

use think\Controller;
use app\common\controller\Common;
use app\common\model\User as UserModel;

class User extends Common
{
    public function userlist()
    {
        $data = input('get.');

        $where = [];

        $start = input('start') ? strtotime(input('start')) : 0;

        $end = input('end') ? strtotime(input('end'))+86400 : time();

        $where['ctime'] = [['>=',$start],['<=',$end]];

        if(input('nickname')){
            $where['nickname'] = ['like','%'.trim(input('nickname')).'%'];
        } 

        $users  =UserModel::where($where)->paginate(2,false,['query'=>$data]);

        $this->assign('users',$users);

        $this->assign('data',$data);
        
        return $this->fetch();
    }

    public function stop()
    {
        $uid = input('uid');

        $data['uid'] = $uid;

        $status = input('status');

        if($status=='false'){
            $data['status'] = '1';
        }else{
            $data['status'] = '0';
        }

        $user = new UserModel;

        $res = $user->save($data,['uid' => $uid]);

        if($res){
            $this->success('更新成功');
        }else{
            $this->error('更新失败');
        }
    }

    public function password()
    {
        $uid = input('uid');

        $user = UserModel::get($uid);

        $this->assign('user',$user);

        return $this->fetch();
    }
}