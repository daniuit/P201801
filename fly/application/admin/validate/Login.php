<?php
namespace app\admin\validate;
use think\Db;
use app\index\model\User;
use think\Validate;
class Login extends Validate
{
    protected $rule = [
        'captcha'  => 'require|captcha',
        'nickname'  =>  'require',
        'password' =>'require|checkLogin'
    ];

    public function checkLogin($password,$rule,$data)
    {

        $where['password'] = md5($password);
        $where['nickname'] = $data['nickname'];
        
       $user = User::get($where);

       if($user){
            if($user->is_admin==1){
                session('is_admin',true);
                session('a_uid',$user->uid);
                session('a_nickname',$user->nickname);
                session('a_face',$user->face);
                return true;
            }else{
                return "非管理员不能登录";
            }
       }else{
          return '用户密码错误'; 
       }

    }


}