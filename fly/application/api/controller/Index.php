<?php

namespace app\api\controller;

use think\Controller;
use think\Db;
/**
* 
*/
class Index extends Controller
{
    
    public function getUser()
    {

        $uid = input('uid');

        if($uid){

            $user = Db::table('fly_user')->find($uid);

            if($user){
                echo json_encode(['code'=>0,'msg'=>'成功','data'=>$user]);exit;

            }else{
                echo json_encode(['code'=>1,'msg'=>'用户不存在']);exit;

            }

        }else{
            echo json_encode(['code'=>1,'msg'=>'接口参数错误,缺少uid']);exit;
        }
        
    }
}