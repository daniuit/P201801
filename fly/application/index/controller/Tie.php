<?php
namespace app\index\controller;

use think\Controller;
use app\index\model\Vercode;
use app\index\model\User;
use think\Loader;
use app\index\model\Category;
use app\index\model\Question;
use app\index\model\Collect;
use app\index\model\Msg;
use think\Db;
/**
 * 
 */
class Tie extends Common
{
    /**
     * [index 帖子详情页]
     * @return [type] [description]
     */
    public function index()
    {

        // $qid = input('get.qid','','int');

        // $qid = $_GET['qid'];

        // var_dump($qid);

        // $question = Question::get($qid);

        // var_dump($question);

        // echo Question::getLastSql();

        // $sql = "select * from question where qid ='$qid'";

        // var_dump($qid);

        // var_dump($sql);

        // exit;

        //获取路径上传的qid
        $qid = input('qid','','int');

        // 给当前帖子加浏览量
        (new Question)->where('qid', $qid)->setInc('view');

        $uid = session('uid') ? session('uid') : 'null';

        //联表查询帖子数据 
        $tie= Db::table('fly_question')
        ->alias('t1')
        ->join('fly_category t2','t1.cid=t2.cid')
        ->join('fly_user t3','t1.uid=t3.uid')
        ->join('fly_collect t4','t4.qid=t1.qid and t4.uid='.$uid,'LEFT')
        ->field('t1.*,t2.cname,t3.nickname,t3.face,t4.coid')
        ->where('t1.qid',$qid)
        ->select();


        $tie = current($tie);
        //解扩展信息
        $tie['ext'] = json_decode($tie['ext'],true);


        $repalys= Db::table('fly_replay')
        ->alias('t1')
        ->join('fly_user t2','t1.uid=t2.uid')
        ->field('t1.*,t2.nickname,t2.face,t3.rzid')
        ->join('fly_replay_zan t3','t3.rid=t1.rid and t3.uid='.$uid,'LEFT')
        ->where('t1.qid',$qid)
        ->order('t1.ctime desc')
        ->select();

        //取分类
        $cates = (new Category)->getALl();
        
        $this->assign('cates',$cates);

        $this->assign('tie',$tie);

        $this->assign('title',$tie['title']);

        $this->assign('replays',$repalys);

        $this->assign('hotTie',(new Question)->getHot());

        return $this->fetch();
    }
    /**
     * [add 帖子增加页面]
     */
    public function add()
    {
        
        //取分类
        $cates = Category::all(['status'=>0]);
        //取人类验证
        $question = (new Vercode())->getRandone();

        $this->assign('question',$question);
        $this->assign('cates',$cates);
        // 引视图;
        return $this->fetch();

    }
    /**
     * [addpost 增加帖子]
     * @return [type] [description]
     */
    public function addpost()
    { 
        
        //接数据
        $data = input('post.','','htmlspecialchars');
        //验证
        $validate = Loader::validate('Tie');

        if(!$validate->check($data)){
            $this->error($validate->getError());
        }

        $data['uid'] = session('uid');

        $data['ctime'] = time();

        //处理提问的扩展信息
        if($data['cid']==1){
            $ext['project'] = $data['project'];
            $ext['version'] = $data['version'];
            $ext['browser'] = $data['browser'];
            $data['ext'] = json_encode($ext);
        }

        //往数据库增加数据 
        $user = (new Question);

        $user->data($data);
        
        $user->save();

        if(!$user){
            $this->error('发布失败');
        }else{
            //给对应的人扣飞吻
            (new User)->where('uid', session('uid'))->setDec('kiss', $data['kiss']);

            $this->success('发布成功',url('index/tie/index',['qid'=> $user->qid]));
        }
    }
    /**
     * [upload 富文本编辑器图片上传接口]
     * @return [type] [description]
     */
    public function upload()
    {
        $file = request()->file('file');

        // 移动到框架应用根目录/public/uploads/ 目录下
        if($file){

            $info = $file->validate(['size'=>500000,'ext'=>'jpg,png,gif'])->move('./tie');

            if($info){
                
                $face ='/tie/'.date("Ymd").'/'.$info->getFilename();
                

                $this->error('上传成功','',['src'=>$face]);

            }else{

                $this->error($file->getError());
                // 上传失败获取错误信息
            }
        }else{
            $this->error('上传失败');
        }
    }
    /**
     * [collect 收藏]
     * @return [type] [description]
     */
    public function collect(){

        

        $data = input('post.');

        $data['uid'] = session('uid');

        $res = Collect::get($data);

        // 判断是否有点赞
        if($res){

            Collect::destroy($data);

            $this->success('取消收藏');

        }else{

            $data['ctime'] = time();

            $user = new Collect;
            $user->data($data);
            $res = $user->save();
        
            $data['ctime'] = time();

            $data['qid'] = $data['qid'];

            $data['type']='collect';

            $data['suid'] = session('uid');

            $question = Question::get($data['qid']);

            $data['duid'] = $question->uid;

            $msg = new Msg();

            $msg->data($data);

            $msg->save();

            $this->success('收藏成功');
        }
    }

}