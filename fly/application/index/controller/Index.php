<?php
namespace app\index\controller;
use think\Controller;
use app\index\model\Question;
use app\index\model\Category;
use app\index\model\Replay;
use app\index\model\Sign;
use app\index\model\User;
use think\Db;
use app\common\controller\Common;

class Index extends Common
{
    public function index()
    {

        


        //联表查询帖子数据 
        $hotReplay= (new Replay)->getHotWeek();


        $where = [];

        $category = [];

        if(input('cid')){
            $where['t2.py'] = input('cid');
            $category['cid'] = input('cid');

        }

        if(input('status')==='0' || input('status')==='1'){
            $where['t1.status'] = input('status');
        }

        if(input('status')==='2'){
            $where['t1.is_jing'] = '1';
        }


        if(empty($where)){
             //联表查询帖子数据 
            $top= Db::table('fly_question')
            ->alias('t1')
            ->join('fly_category t2','t1.cid=t2.cid')
            ->join('fly_user t3','t1.uid=t3.uid')
            ->field('t1.*,t2.cname,t3.nickname,t3.face')
            ->where('t1.is_top','1')
            ->select();

            $this->assign('top',$top);
        }


        $allTie= Db::table('fly_question')
        ->alias('t1')
        ->join('fly_category t2','t1.cid=t2.cid')
        ->join('fly_user t3','t1.uid=t3.uid')
        ->field('t1.*,t2.cname,t3.nickname,t3.face')
        ->where($where)
        ->paginate(5);

         
        // select 
        //  count(t1.rid) cn,t2.nickname
        // from fly_replay t1 
        // inner join fly_user t2 on t1.uid=t2.uid 
        // where t1.ctime>0 
        // group by t2.uid 
        // order by cn asc
        
        //取分类
        $cates = (new Category)->getALl();

        $this->assign('hotReplay',$hotReplay);
        $this->assign('allTie',$allTie);
        $this->assign('where',$where);
        $this->assign('category',$category);
        $this->assign('cates',$cates);
        $this->assign('title',config('site.title'));
        $this->assign('hotTie',(new Question)->getHot());


        $signInfo = (new Sign())->getUserSignInfo();

        $this->assign('signInfo',$signInfo);

        var_dump($signInfo);
        
        return $this->fetch();
    }

    public function sign()
    {
        $uid = session('uid');

        $sign = Sign::get(['uid'=>$uid,'date'=>date('Y-m-d')]);

        if($sign){
            $this->error('今天已经签到，不能重复签到');
        }

        $signInfo = (new Sign())->getUserSignInfo();

        Sign::destroy(['uid' => $uid]);

        $data['uid'] = $uid;

        $data['stime'] = time();

        $data['num'] = $signInfo['num']+1;

        $data['date'] = date('Y-m-d');

        $res = (new Sign())->data($data)->save();

        (new User)->where('uid', $uid)->setInc('view', $signInfo['kiss']);

        if($res){
            $this->success('签到成功');
        }else{
            $this->error('签到失败');
        }
    }

    public function getCate()
    {
        $cates = Db::table('fly_category')->select();


        $data = $this->getCateTree($cates);


        $this->success('ok','',$data);
    }

    public function getCateTree($cates,$fid=0)
    {
        $temp = [];
        foreach ($cates as $row) {
            if($row['fid']==$fid){
                $row['son'] = $this->getCateTree($cates,$row['cid']);
                $temp [] = $row;
            }
        }
        return $temp;
    }
}
