<?php
namespace app\index\controller;

use think\Controller;
use app\index\model\Vercode;
use app\index\model\User;
use think\Loader;

class Reg extends Common
{
    public function index()
    {
        $this->view->engine->layout(true);
        $question = (new Vercode())->getRandone();

        $this->assign('question',$question);
        $this->assign('title',"注册");
        // 引视图;
        return $this->fetch();
    }

    public function checkdata()
    {

        $this->checkGt();

        $data = input('post.');

        $validate = Loader::validate('Reg');

        if(!$validate->check($data)){
            $this->error($validate->getError());
        }

        $data['password'] = md5($data['password']);

        $data['ctime'] = time();

        // Db::table('fly_user')->insert($data);

        $user = (new User)->data($data)->save();


        if(!$user){
            $this->error('注册失败');
        }else{
            $this->success('注册成功');
        }
    }

    public function gt()
    {

        define("CAPTCHA_ID", "f953730f6eb6a61160710d0e097afb39");
        define("PRIVATE_KEY", "70e14907d0e4bcad0840c1e380f9c337");

        require_once '../vendor/gt/class.geetestlib.php';

        $GtSdk = new \GeetestLib(CAPTCHA_ID, PRIVATE_KEY);

        session_start();

        $data = array(
                "user_id" => "test", # 网站用户id
                "client_type" => "web", #web:电脑上的浏览器；h5:手机上的浏览器，包括移动应用内完全内置的web_view；native：通过原生SDK植入APP应用的方式
                "ip_address" => "127.0.0.1" # 请在此处传输用户请求验证时所携带的IP
            );

        $status = $GtSdk->pre_process($data, 1);

        $_SESSION['gtserver'] = $status;

        $_SESSION['user_id'] = $data['user_id'];

        echo $GtSdk->get_response_str();
    }

    public function checkGt()
    {
        
        // $_POST['geetest_challenge']= '3409534095834095';


        define("CAPTCHA_ID", "f953730f6eb6a61160710d0e097afb39");
        define("PRIVATE_KEY", "70e14907d0e4bcad0840c1e380f9c337");

        require_once '../vendor/gt/class.geetestlib.php';

        session_start();

        $GtSdk = new \GeetestLib(CAPTCHA_ID, PRIVATE_KEY);


        $data = array(
                "user_id" => $_SESSION['user_id'], # 网站用户id
                "client_type" => "web", #web:电脑上的浏览器；h5:手机上的浏览器，包括移动应用内完全内置的web_view；native：通过原生SDK植入APP应用的方式
                "ip_address" => "127.0.0.1" # 请在此处传输用户请求验证时所携带的IP
            );

        if ($_SESSION['gtserver'] == 1) {   //服务器正常
            $result = $GtSdk->success_validate($_POST['geetest_challenge'], $_POST['geetest_validate'], $_POST['geetest_seccode'], $data);
            if ($result) {
                // $this->success("success");
            } else{
                $this->error("极验证不通过");
            }
        }else{  //服务器宕机,走failback模式
            if ($GtSdk->fail_validate($_POST['geetest_challenge'],$_POST['geetest_validate'],$_POST['geetest_seccode'])) {
                // $this->success("success");
            }else{
                $this->error("极验证不通过");
            }
        }
    }

}