<?php
namespace app\index\controller;

use think\Controller;
use app\index\model\Vercode;
use app\index\model\User;
use think\Loader;
use phpmailer\Mail;
use alimsg\Phone;
use Wuliu\Wuliu;
use Wuliu\Wl;
use app\common\controller\Common;

class Login extends Common
{
    public function index()
    {
        $question = (new Vercode())->getRandone();

        $this->assign('question',$question);

        $this->assign('title',"登录");
        // 引视图;
        return $this->fetch();
    }

    public function checkdata()
    {
        $data = input('post.');

        $validate = Loader::validate('Login');

        if(!$validate->check($data)){
            $this->error($validate->getError());
        }

        $this->success('登录成功');
    }

    public function loginout()
    {
        session(null);

        $this->success("退出成功",url('index/index/index'));
    }


    public function getCity()
    {
        $ip = "61.148.gfh.52";

        $url = "http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;

        $data = file_get_contents($url);

        var_dump(json_decode($data,true));

        'uqezamecauhvbhhd';
    }


    public function sendmail()
    {
        
        Mail::send('627556311@qq.com','测试','这个是一个测试的邮件');

        // sendMail('408640017@qq.com','测试xxxx','这个是一个测试的邮件');

    }

    public function phone()
    {
       
       $res =Phone::send(18925139194,'小xxxx明xxx',mt_rand(34567,78945));

       var_dump($res);
    }

    public function get()
    {
        // $res = Wuliu::get(780098068058,'ZTO');

        // $res = Wl::get(780098068058,'ZTO');
        // 
        $res = \tq\Tq::get('北京');

        var_dump($res);
    }

    public function qqlogin()
    {
        qqlogin();
    }

    public function qqreturn()
    {
       $openId = getOpenId();

       $user = User::get(['openid'=>$openId]);

       if($user){
            session('uid',$user->uid);
            session('nickname',$user->nickname);
            session('face',$user->face);
            $this->success('登录成功',url('index/index/index'));
       }else{
        

            $userInfo = getUserInfo();

            $userInfo['sex'] = $userInfo['gender'];

            $userInfo['ctime'] = time();

            $userInfo['openid'] =  $openId;

            $userInfo['face']  = $this->getQqFace($userInfo['figureurl_2']);
 
            $user = new User();

            $user->data($userInfo);

            $res = $user->save();

            if($res){

                session('uid',$user->uid);
                session('nickname',$user->nickname);
                session('face',$user->face);

                $this->success('登录成功',url('index/index/index'));

            }


       }
    }

    /**
     * [getQqFace description]
     * @param  [type] $url [description]
     * @return [type]      [description]
     */
    public function getQqFace($url)
    {
        
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HEADER, false);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $data = curl_exec($curl);

        $path = "./uploads/".date('Ymd');

        is_dir($path) || mkdir($path);

        $face = $path.'/'.uniqid().'.png';

        file_put_contents($face,$data);

        return $face;
    }



}