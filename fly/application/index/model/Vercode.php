<?php

namespace app\index\model;
/**
 * 验证码模型
 */
use think\Model;
use think\Db;
class Vercode extends Model
{
    /**
     * [getRandOne 随机获取一条数据]
     * @return [type] [description]
     */
    public function getRandOne()
    {
        $data = Db::query("select * from fly_vercode order by rand() limit 1");

        $data = current($data);

        session('answer',$data['answer']);

        return $data['question'];
    }
}