<?php

namespace app\index\model;
/**
 * 分类模型
 */
use think\Model;
class Category extends Model
{
    public function getALl()
    {
        if(cache('cates')){
            return cache('cates');

        }else{
            $cates = Category::all();

            cache('cates',$cates,36000);

            return cache('cates');
        }
    }
}