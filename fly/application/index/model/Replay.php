<?php

namespace app\index\model;

use think\Db;
/**
 * 用户模型
 */
use think\Model;
class Replay extends Model
{
    public function getHotWeek()
    {
        if(cache('getHotWeek')){
            return cache('getHotWeek');

        }else{
            $hotReplay= Db::table('fly_replay')
            ->alias('t1')
            ->join('fly_user t2','t1.uid=t2.uid')
            ->field('count(t1.rid) cn,t2.nickname,t2.face,t2.uid')
            ->group('t2.uid')
            ->where('t1.ctime','>',strtotime('-7days'))
            ->order('cn desc')
            ->select();

            cache('getHotWeek',$hotReplay,36000);

            return cache('getHotWeek');
        }
        
    }
}