<?php

namespace app\index\model;
/**
 * 用户模型
 */
use think\Model;
class Sign extends Model
{
    public function getUserSignInfo()
    {
        $data = [];

        if(session('uid')){

            $sign = $this->where(['uid'=>session('uid'),'date'=>date("Y-m-d")])->find();

            if($sign){
                $data['num'] = $sign->num;
                $data['kiss'] = $this->getKiss($sign->num); 
                $data['today'] = true;
            }else{

                 $user = $this->where(['uid'=>session('uid'),'date'=>date("Y-m-d",strtotime('-1days'))])->find();

                if($user){
                    $data['num'] = $user->num;
                    $data['kiss'] = $this->getKiss($user->num+1); 
                    $data['today'] = false;
                }else{
                    $data['num'] = 0;
                    $data['kiss'] = 5;
                    $data['today'] = false;
                }
            }
        }

        return $data;
    }

    public function getKiss($num)
    {
        if($num<=5){
            return 5;
        }elseif($num<=10){
            return 10;
        }elseif($num<=20){
            return 20;
        }else{
            return 30;
        }
    }
}