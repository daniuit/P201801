<?php

namespace app\index\model;
/**
 * 帖子模型
 */
use think\Model;
class Question extends Model
{   
    /**
     * [getHot 获取热闹的帖子]
     * @param  integer $num [description]
     * @return [type]       [description]
     */
    public function getHot($num=10)
    {
        return $this->order('replay desc')->limit($num)->select();
    }
}