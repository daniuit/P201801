<?php
namespace app\index\validate;
use app\index\model\User;

class Replay extends Base
{
    protected $rule = [
        'content' =>'require|min:10',
        'qid' =>'require|number',
    ];
}