<?php
namespace app\index\validate;

class Set extends Base
{
    protected $rule = [
        'sex' => "require|in:男,女",
        'email' =>'require|email',
        'nickname' =>'require|length:6,8|unique:user',
        'phone' => 'require|regex:1\d{10}|unique:user'
    ];
}