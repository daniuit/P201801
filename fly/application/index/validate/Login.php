<?php
namespace app\index\validate;
use think\Db;
use app\index\model\User;
class Login extends Base
{
    protected $rule = [
        'vercode'  => 'require|checkVerCode',
        'loginName'  =>  'require',
        'password' =>'require|checkLogin'
    ];

    public function checkLogin($password,$rule,$data)
    {
       $loginName = $data['loginName'];

       $password  = md5($password);

       $preg = '/@/';

       $c = preg_match($preg,$loginName);

       $where = [];

       if($c){
            $where['email'] = $loginName;
       }else{
            $where['phone'] = $loginName;
       }

       $where['password'] = $password;

       // $res = Db::name('user')->where($where)->find();

       $user = User::get($where);

       

       if($user){
          if($user->status=='1'){
              return "用户被禁用，请联系警察解决";
          }
          session('uid',$user->uid);
          session('nickname',$user->nickname);
          session('face',$user->face);
          return true;
       }else{
          return '用户密码错误'; 
       }

    }


}