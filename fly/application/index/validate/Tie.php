<?php
namespace app\index\validate;
use app\index\model\User;

class Tie extends Base
{
    protected $rule = [
        'vercode'  => 'require|checkVerCode',
        'title' =>'require|length:10,30',
        'content' =>'require|min:10',
        'kiss' =>'require|number|checkKiss',
        'cid'=>'require|number'
    ];

    public function checkKiss($kiss)
    {
        $user = User::get(session('uid'));

        if($user->kiss<$kiss){
            return "飞吻不足";
        }else{
            return true;
        }
    }

}