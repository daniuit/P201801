<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

/**
 * [Ftime 格式化时间]
 * @param [type] $time [时间戳]
 */
function Ftime($time=null)
{
    if($time){

        $diff = time()-$time;

        if($diff<60 && $diff>0){
            return "刚刚";
        }elseif($diff<3600){
            return floor($diff/60)."分钟之前";
        }elseif($diff<86400){
            return floor($diff/3600)."小时之前";
        }elseif($diff<86400*7){
            return floor($diff/86400)."天之前";
        }else{
            return date("Y-m-d H:i:s",$time);
        }
    }else{
        return date("Y-m-d H:i:s");
    }
}
/**
 * [Fsize 格式化存储单位]
 * @param [type] $size [必传字节数]
 */
function Fsize($size)
{
    if($size<1024){
        return $size."b";
    }elseif($size<pow(1024,2)){
        return round($size/1024,2)."K";
    }elseif($size<pow(1024,3)){
        return round($size/pow(1024,2),2)."M";
    }elseif($size<pow(1024,4)){
        return round($size/pow(1024,3),2)."G";
    }elseif($size<pow(1024,5)){
        return round($size/pow(1024,4),2)."T";
    }
}
/**
 * [getRandNum 随机生成数字]
 * @param  integer $length [数量 ]
 * @param  boolean $repeat [是否重复 true 不重复，false 重复]
 * @param  integer $min    [起始数]
 * @param  integer $max    [结束数]
 * @return [数组]          [随机数组]
 */
function getRandNum($length=7,$repeat=false,$min=1,$max=47)
{
    $tempArr = [];

    for ($i=0; $i < $length ; $i++) { 
        $num = mt_rand($min,$max);
        if($repeat){

            if(in_array($num,$tempArr)){
                $i--;
            }else{
                $tempArr[] = $num;
            }

        }else{
            $tempArr[] = $num;
        }
    }
    return $tempArr;
}

/**
 * [delDir 递归删除目录]
 * @param  [type] $dir [目录名]
 * @return [type]      [无返回值]
 */
function delDir($dir)
{
    $files = glob($dir.'/*');

    foreach ($files as  $file) {
        if(is_dir($file)){
            delDir($file);
        }else{
            unlink($file);
        }
    }

    rmdir($dir);
}

function sendMail($email,$title,$content)
{
    

    require '../vendor/phpmailer/PHPMailerAutoload.php';

    $mail = new PHPMailer;

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.qq.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = '113664000@qq.com';                 // SMTP username
    $mail->Password = 'uqezamecauhvbhhd';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to

    $mail->setFrom('113664000@qq.com', '老司机论坛');
    $mail->addAddress($email); 

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $title;
    $mail->Body    = $content;

    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent';
    }
}

function qqlogin()
{
    require_once("../vendor/qqConnect/Api/qqConnectAPI.php");

    $qc = new QC();
    $qc->qq_login();
}

function getOpenId()
{
    require_once("../vendor/qqConnect/Api/qqConnectAPI.php");

    $qc = new QC();

    $qc->qq_callback();

    return $qc->get_openid();
}

function getUserInfo()
{
    require_once("../vendor/qqConnect/Api/qqConnectAPI.php");
    $qc = new QC();
    
    return $arr = $qc->get_user_info();
}

function is_login()
{
    if(!session('uid')){
            $this->error('你还没登录，请先登录');
        }
}
