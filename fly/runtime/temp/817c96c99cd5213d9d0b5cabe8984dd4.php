<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:89:"C:\phpStudy\PHPTutorial\WWW\P201801\fly\public/../application/index\view\login\index.html";i:1525068642;s:74:"C:\phpStudy\PHPTutorial\WWW\P201801\fly\application\index\view\layout.html";i:1526020551;}*/ ?>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><?php echo !empty($title)?$title:" "; ?>-<?php echo config('site.site_name'); ?></title>
        <link rel="stylesheet" href="//at.alicdn.com/t/font_24081_dddajmj0coc4n29.css?id=<?php echo config('site.tt'); ?>">
        <link rel="stylesheet" href="/static/layui/css/layui.css?id=<?php echo config('site.tt'); ?>">
        <link rel="stylesheet" href="/static/index/css/global.css?id=<?php echo config('site.tt'); ?>" charset="utf-8">
        <link rel="stylesheet" href="/static/index/css/main.css?id=<?php echo config('site.tt'); ?>" charset="utf-8">
        <script src="/static/layui/layui.js?id=<?php echo config('site.tt'); ?>"></script>
    </head>
    
    <body>
        <div class="fly-header layui-bg-black">
            <div class="layui-container">
                <a class="fly-logo" href="/">
                    <img src="//res.layui.com/static/images/fly/logo.png?id=<?php echo config('site.tt'); ?>" alt="layui"></a>
                <ul class="layui-nav fly-nav layui-hide-xs">
                    <li class="layui-nav-item">
                        <a href="/">
                            <i class="iconfont icon-jiaoliu"></i>交流9999</a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="javascript:;">
                            <i class="iconfont icon-chanpin" style="top: 1px;"></i>专区</a>
                        <dl class="layui-nav-child fly-nav-child">
                            <dd>
                                <a href="/vipclub/list/layuiadmin/">layuiAdmin</a></dd>
                            <hr>
                            <dd>
                                <a href="/vipclub/list/layim/">LayIM</a></dd>
                        </dl>
                        <span class="layui-badge-dot" style="margin: -5px 10px 0; right: 0;"></span>
                    </li>
                    <li class="layui-nav-item">
                        <a href="http://www.layui.com/">
                            <i class="iconfont icon-ui"></i>框架</a>
                    </li>
                </ul>

                <?php if(session('uid')): ?>
                    <ul class="layui-nav fly-nav-user">
                        <li class="layui-nav-item">
                            <a class="fly-nav-avatar" href="/user/" id="LAY_header_avatar">
                                <cite class="layui-hide-xs"><?php echo session('nickname'); ?></cite>
                                <img src="/<?php echo session('face'); ?>">
                                <span class="layui-nav-more"></span>
                            </a>
                            <dl class="layui-nav-child layui-anim layui-anim-upbit">
                                <dd>
                                    <a href="<?php echo url('index/user/index'); ?>">
                                        <i class="layui-icon"></i>用户中心</a></dd>
                                <dd>
                                    <a href="<?php echo url('index/user/set'); ?>">
                                        <i class="layui-icon"></i>基本设置</a></dd>
                                <hr>
                                <dd>
                                    <a href="<?php echo url('index/seting/msg'); ?>">
                                        <i class="iconfont icon-tongzhi" style="top: 4px;"></i>我的消息</a>
                                </dd>
                                <dd>
                                    <a href="<?php echo url('index/user/index'); ?>">
                                        <i class="layui-icon" style="margin-left: 2px; font-size: 22px;"></i>我的主页</a></dd>
                                <hr style="margin: 5px 0;">
                                <dd>
                                    <a href="<?php echo url('index/login/loginout'); ?>" style="text-align: center;">退出</a></dd>
                            </dl>
                        </li>
                    </ul>
                <?php else: ?> 
                    <ul class="layui-nav fly-nav-user">
                        <li class="layui-nav-item">
                            <a class="iconfont icon-touxiang layui-hide-xs" href="/user/login/"></a>
                        </li>
                        <li class="layui-nav-item">
                            <a href="<?php echo url('index/login/index'); ?>">登入</a></li>
                        <li class="layui-nav-item">
                            <a href="<?php echo url('index/reg/index'); ?>">注册</a></li>
                        <li class="layui-nav-item layui-hide-xs">
                            <a href="<?php echo url('index/login/qqlogin'); ?>"  title="QQ登入" class="iconfont icon-qq"></a>
                        </li>
                        <li class="layui-nav-item layui-hide-xs">
                            <a href="/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a>
                        </li>
                    </ul>
                <?php endif; ?>
            </div>
        </div>

<div class="layui-container fly-marginTop">
    <div class="fly-panel fly-panel-user" pad20>
        <div class="layui-tab layui-tab-brief" lay-filter="user">
            <ul class="layui-tab-title">
                <li class="layui-this">登入</li>
                <li>
                    <a href="/user/reg/">注册</a></li>
            </ul>
            <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                <div class="layui-tab-item layui-show">
                    <div class="layui-form layui-form-pane">
                        <form method="post">
                            <div class="layui-form-item">
                                <label for="L_email" class="layui-form-label">手机/邮箱</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_loginName" name="loginName" required lay-verify="required" autocomplete="off" class="layui-input"></div>
                                <div class="layui-form-mid layui-word-aux">使用手机或者邮箱中的任意一个均可（若采用手机，请确保你的帐号已绑定过该手机）</div></div>
                            <div class="layui-form-item">
                                <label for="L_pass" class="layui-form-label">密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" id="L_pass" name="password" required lay-verify="required" autocomplete="off" class="layui-input"></div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_vercode" class="layui-form-label">人类验证</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_vercode" name="vercode" required lay-verify="required" placeholder="请回答后面的问题" autocomplete="off" class="layui-input"></div>
                                <div class="layui-form-mid">
                                    <span style="color: #c00;"><?php echo $question; ?></span></div>
                            </div>
                            <div class="layui-form-item">
                                <button class="layui-btn" lay-filter="login" lay-submit>立即登录</button>
                                <span style="padding-left:20px;">
                                    <a href="/user/forget">忘记密码？</a></span>
                            </div>
                            <div class="layui-form-item fly-form-app">
                                <span>或者使用社交账号登入</span>
                                <a href="/app/qq" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" class="iconfont icon-qq" title="QQ登入"></a>
                                <a href="/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" class="iconfont icon-weibo" title="微博登入"></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
//Demo
layui.use(['form','jquery'], function(){
  var form = layui.form;
  $ = layui.jquery;
  //监听提交
  form.on('submit(login)', function(data){
    $.ajax({
        url: '<?php echo url('index/login/checkdata'); ?>',
        type: 'POST',
        dataType: 'json',
        data: data.field,
    })
    .done(function(res) {
       if (res.code==0) {
            layer.msg(res.msg,function(){});
       }else{
            layer.alert(res.msg,function(){
                location.href="<?php echo url('index/index/index'); ?>";
            });
       }
    })
    .fail(function() {
        layer.msg('系统异常',function(){});
    })
    
    return false;
  });
});
</script>
 <div class="fly-footer">
            <p>
                <a href="http://fly.layui.com/">Fly社区</a>2018 &copy;
                <a href="http://www.layui.com/">layui.com</a></p>
            <p>
                <a href="http://fly.layui.com/jie/3147/" target="_blank">付费计划</a>
                <a href="/case/2018/" target="_blank">Layui案例</a>
                <a href="http://www.layui.com/template/fly/" target="_blank">获取Fly社区模版</a>
                <a href="http://fly.layui.com/jie/2461/" target="_blank">微信公众号</a></p>
            <p class="fly-union">
                <a href="https://www.upyun.com?from=layui" target="_blank" rel="nofollow" upyun>
                    <img src="//res.layui.com//static/images/other/upyun.png?t=1"></a>
                <span>提供 CDN 赞助</span></p>
        </div>

        <script>
        //注意：导航 依赖 element 模块，否则无法进行功能性操作
        layui.use('element', function(){
          var element = layui.element;
          
          //…
        });
        </script>
    </body>

</html>