<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:89:"C:\phpStudy\PHPTutorial\WWW\P201801\fly\public/../application/admin\view\login\index.html";i:1526270653;s:74:"C:\phpStudy\PHPTutorial\WWW\P201801\fly\application\admin\view\layout.html";i:1526269605;}*/ ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>后台登录-X-admin2.0</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

</head>
<body class="login-bg">
    
    <div class="login layui-anim layui-anim-up">
        <div class="message">x-admin2.0-管理登录</div>
        <div id="darkbannerwrap"></div>
        
        <form method="post" class="layui-form" >
            <input name="nickname" placeholder="用户名"  type="text" lay-verify="required" class="layui-input" >
            <hr class="hr15">
            <input name="password" lay-verify="required" placeholder="密码"  type="password" class="layui-input">
            <hr class="hr15">
            <input name="captcha" style="width:55%;display: inline-block" lay-verify="required" placeholder="验证码"  type="text" class="layui-input">
            <div class="code" style="width:40%;height: 50px;background: red;display: inline-block;float: right;">
              <img width="100%" height="100%" src="<?php echo url('admin/login/code'); ?>" alt="captcha" />
            </div>
            <hr class="hr15">
            <input value="登录" lay-submit lay-filter="login" style="width:100%;" type="submit">
            <hr class="hr20" >
        </form>
    </div>

    <script>
      //Demo
      layui.use(['form','jquery'], function(){
        var form = layui.form;
        $ = layui.jquery;
        //监听提交
        form.on('submit(login)', function(data){
          $.ajax({
              url: '<?php echo url('admin/login/checkdata'); ?>',
              type: 'POST',
              dataType: 'json',
              data: data.field,
          })
          .done(function(res) {
             if (res.code==0) {
                  layer.msg(res.msg,function(){});
             }else{
                  layer.alert(res.msg,function(){
                      location.href="<?php echo url('admin/index/index'); ?>";
                  });
             }
          })
          .fail(function() {
              layer.msg('系统异常',function(){});
          })
          
          return false;
        });
      });
      </script>

</body>
</html>