<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:87:"C:\phpStudy\PHPTutorial\WWW\P201801\fly\public/../application/index\view\reg\index.html";i:1527155079;s:74:"C:\phpStudy\PHPTutorial\WWW\P201801\fly\application\index\view\layout.html";i:1526020551;}*/ ?>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><?php echo !empty($title)?$title:" "; ?>-<?php echo config('site.site_name'); ?></title>
        <link rel="stylesheet" href="//at.alicdn.com/t/font_24081_dddajmj0coc4n29.css?id=<?php echo config('site.tt'); ?>">
        <link rel="stylesheet" href="/static/layui/css/layui.css?id=<?php echo config('site.tt'); ?>">
        <link rel="stylesheet" href="/static/index/css/global.css?id=<?php echo config('site.tt'); ?>" charset="utf-8">
        <link rel="stylesheet" href="/static/index/css/main.css?id=<?php echo config('site.tt'); ?>" charset="utf-8">
        <script src="/static/layui/layui.js?id=<?php echo config('site.tt'); ?>"></script>
    </head>
    
    <body>
        <div class="fly-header layui-bg-black">
            <div class="layui-container">
                <a class="fly-logo" href="/">
                    <img src="//res.layui.com/static/images/fly/logo.png?id=<?php echo config('site.tt'); ?>" alt="layui"></a>
                <ul class="layui-nav fly-nav layui-hide-xs">
                    <li class="layui-nav-item">
                        <a href="/">
                            <i class="iconfont icon-jiaoliu"></i>交流9999</a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="javascript:;">
                            <i class="iconfont icon-chanpin" style="top: 1px;"></i>专区</a>
                        <dl class="layui-nav-child fly-nav-child">
                            <dd>
                                <a href="/vipclub/list/layuiadmin/">layuiAdmin</a></dd>
                            <hr>
                            <dd>
                                <a href="/vipclub/list/layim/">LayIM</a></dd>
                        </dl>
                        <span class="layui-badge-dot" style="margin: -5px 10px 0; right: 0;"></span>
                    </li>
                    <li class="layui-nav-item">
                        <a href="http://www.layui.com/">
                            <i class="iconfont icon-ui"></i>框架</a>
                    </li>
                </ul>

                <?php if(session('uid')): ?>
                    <ul class="layui-nav fly-nav-user">
                        <li class="layui-nav-item">
                            <a class="fly-nav-avatar" href="/user/" id="LAY_header_avatar">
                                <cite class="layui-hide-xs"><?php echo session('nickname'); ?></cite>
                                <img src="/<?php echo session('face'); ?>">
                                <span class="layui-nav-more"></span>
                            </a>
                            <dl class="layui-nav-child layui-anim layui-anim-upbit">
                                <dd>
                                    <a href="<?php echo url('index/user/index'); ?>">
                                        <i class="layui-icon"></i>用户中心</a></dd>
                                <dd>
                                    <a href="<?php echo url('index/user/set'); ?>">
                                        <i class="layui-icon"></i>基本设置</a></dd>
                                <hr>
                                <dd>
                                    <a href="<?php echo url('index/seting/msg'); ?>">
                                        <i class="iconfont icon-tongzhi" style="top: 4px;"></i>我的消息</a>
                                </dd>
                                <dd>
                                    <a href="<?php echo url('index/user/index'); ?>">
                                        <i class="layui-icon" style="margin-left: 2px; font-size: 22px;"></i>我的主页</a></dd>
                                <hr style="margin: 5px 0;">
                                <dd>
                                    <a href="<?php echo url('index/login/loginout'); ?>" style="text-align: center;">退出</a></dd>
                            </dl>
                        </li>
                    </ul>
                <?php else: ?> 
                    <ul class="layui-nav fly-nav-user">
                        <li class="layui-nav-item">
                            <a class="iconfont icon-touxiang layui-hide-xs" href="/user/login/"></a>
                        </li>
                        <li class="layui-nav-item">
                            <a href="<?php echo url('index/login/index'); ?>">登入</a></li>
                        <li class="layui-nav-item">
                            <a href="<?php echo url('index/reg/index'); ?>">注册</a></li>
                        <li class="layui-nav-item layui-hide-xs">
                            <a href="<?php echo url('index/login/qqlogin'); ?>"  title="QQ登入" class="iconfont icon-qq"></a>
                        </li>
                        <li class="layui-nav-item layui-hide-xs">
                            <a href="/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a>
                        </li>
                    </ul>
                <?php endif; ?>
            </div>
        </div>

<div class="layui-container fly-marginTop">
    <div class="fly-panel fly-panel-user" pad20>
        <div class="layui-tab layui-tab-brief" lay-filter="user">
            <ul class="layui-tab-title">
                <li>
                    <a href="/user/login/">登入</a></li>
                <li class="layui-this">注册</li></ul>
            <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                <div class="layui-tab-item layui-show">
                    <div class="layui-form layui-form-pane">
                        <form method="post">
                            <div class="layui-form-item">
                                <label for="L_email" class="layui-form-label">手机</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_phone" name="phone" required lay-verify="phone" autocomplete="off" class="layui-input"></div>
                            </div>
                            <!-- <div class="layui-form-item">
                                <label for="L_vercode" class="layui-form-label">验证码</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_vercode" name="vercode" required lay-verify="required" placeholder="请输入手机验证码" autocomplete="off" class="layui-input"></div>
                                <div class="layui-form-mid" style="padding: 0!important;">
                                    <button type="button" class="layui-btn layui-btn-normal" id="FLY-getvercode">获取验证码</button></div>
                            </div> -->
                            <div class="layui-form-item">
                                <label for="L_username" class="layui-form-label">昵称</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_username" name="nickname" required lay-verify="required" autocomplete="off" class="layui-input"></div>
                                <div class="layui-form-mid layui-word-aux">你在社区的名字</div></div>
                            <div class="layui-form-item">
                                <label for="L_pass" class="layui-form-label">密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" id="L_pass" name="password" required lay-verify="required" autocomplete="off" class="layui-input"></div>
                                <div class="layui-form-mid layui-word-aux">6到16个字符</div></div>
                            <div class="layui-form-item">
                                <label for="L_repass" class="layui-form-label">确认密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" id="L_repass" name="repassword" required lay-verify="required" autocomplete="off" class="layui-input"></div>
                            </div>
                            <div class="layui-form-item">
                            <label for="L_vercode" class="layui-form-label">人类验证</label>
                            <div class="layui-input-inline">
                                <input type="text" id="L_vercode" name="vercode" required="" lay-verify="required" placeholder="请回答后面的问题" autocomplete="off" class="layui-input"></div>
                            <div class="layui-form-mid">
                                <span style="color: #c00;"><?php echo $question; ?></span></div>
                        </div>
                            <div class="layui-form-item">
                                <div id="embed-captcha"></div>
                                <p id="wait" class="show">正在加载验证码......</p>
                                <p id="notice" class="hide">请先完成验证</p>
                            </div>
                            <div class="layui-form-item" style="position: relative; left: -10px; height: 32px;">
                                <input type="checkbox" name="agreement" lay-skin="primary" title="" checked>
                                <a href="/instructions/terms.html" target="_blank" style="position: relative; top: 4px; left: 5px; color: #999;">同意用户服务条款</a></div>
                            <div class="layui-form-item">
                                <button class="layui-btn" lay-filter="reg" lay-submit>立即注册</button></div>
                            <div class="layui-form-item fly-form-app">
                                <span>或者直接使用社交账号快捷注册</span>
                                <a href="/app/qq" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" class="iconfont icon-qq" title="QQ登入"></a>
                                <a href="/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" class="iconfont icon-weibo" title="微博登入"></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .inp {
        border: 1px solid gray;
        padding: 0 10px;
        width: 200px;
        height: 30px;
        font-size: 18px;
    }
    .btn {
        border: 1px solid gray;
        width: 100px;
        height: 30px;
        font-size: 18px;
        cursor: pointer;
    }
    #embed-captcha {
        width: 300px;
    }
    .show {
        display: block;
    }
    .hide {
        display: none;
    }
    #notice {
        color: red;
    }
    </style>
<script src="http://apps.bdimg.com/libs/jquery/1.9.1/jquery.js"></script>
<script src="/static/index/js/gt.js"></script>
<script>
    var handlerEmbed = function (captchaObj) {
        $("#embed-submit").click(function (e) {
            var validate = captchaObj.getValidate();
            if (!validate) {
                $("#notice")[0].className = "show";
                setTimeout(function () {
                    $("#notice")[0].className = "hide";
                }, 2000);
                e.preventDefault();
            }
        });
        // 将验证码加到id为captcha的元素里，同时会有三个input的值：geetest_challenge, geetest_validate, geetest_seccode
        captchaObj.appendTo("#embed-captcha");
        captchaObj.onReady(function () {
            $("#wait")[0].className = "hide";
        });
        // 更多接口参考：http://www.geetest.com/install/sections/idx-client-sdk.html
    };
    $.ajax({
        // 获取id，challenge，success（是否启用failback）
        url: "<?php echo url('index/reg/gt'); ?>?t=" + (new Date()).getTime(), // 加随机数防止缓存
        type: "get",
        dataType: "json",
        success: function (data) {


            console.log(data);
            // 使用initGeetest接口
            // 参数1：配置参数
            // 参数2：回调，回调的第一个参数验证码对象，之后可以使用它做appendTo之类的事件
            initGeetest({
                gt: data.gt,
                challenge: data.challenge,
                new_captcha: data.new_captcha,
                product: "embed", // 产品形式，包括：float，embed，popup。注意只对PC版验证码有效
                offline: !data.success // 表示用户后台检测极验服务器是否宕机，一般不需要关注
                // 更多配置参数请参见：http://www.geetest.com/install/sections/idx-client-sdk.html#config
            }, handlerEmbed);
        }
    });
</script>
<script>
    //Demo
    layui.use(['form'], function(){
      var form = layui.form;
      // $ = layui.jquery;
      //监听提交
      form.on('submit(reg)', function(data){
        $.ajax({
            url: '<?php echo url('index/reg/checkdata'); ?>',
            type: 'POST',
            dataType: 'json',
            data: data.field,
        })
        .done(function(res) {
           if (res.code==0) {
                layer.msg(res.msg,function(){});
           }else{
                layer.alert(res.msg,function(){
                    location.href="<?php echo url('index/login/index'); ?>";
                });
           }
        })
        .fail(function() {
            layer.msg('系统异常',function(){});
        })
        
        return false;
      });
    });
    </script>
 <div class="fly-footer">
            <p>
                <a href="http://fly.layui.com/">Fly社区</a>2018 &copy;
                <a href="http://www.layui.com/">layui.com</a></p>
            <p>
                <a href="http://fly.layui.com/jie/3147/" target="_blank">付费计划</a>
                <a href="/case/2018/" target="_blank">Layui案例</a>
                <a href="http://www.layui.com/template/fly/" target="_blank">获取Fly社区模版</a>
                <a href="http://fly.layui.com/jie/2461/" target="_blank">微信公众号</a></p>
            <p class="fly-union">
                <a href="https://www.upyun.com?from=layui" target="_blank" rel="nofollow" upyun>
                    <img src="//res.layui.com//static/images/other/upyun.png?t=1"></a>
                <span>提供 CDN 赞助</span></p>
        </div>

        <script>
        //注意：导航 依赖 element 模块，否则无法进行功能性操作
        layui.use('element', function(){
          var element = layui.element;
          
          //…
        });
        </script>
    </body>

</html>