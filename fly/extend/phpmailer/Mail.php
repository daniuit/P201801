<?php
namespace phpmailer;

use phpmailer\src\PHPMailer;

class Mail 
{
    public static function send($email,$title,$html)
    {

        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = config('mail.host');  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = config('mail.username');                 // SMTP username
            $mail->Password = config('mail.password');                           // SMTP password
            $mail->SMTPSecure = config('mail.secure');                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = config('mail.port');                                    // TCP port to connect to

            //Recipients
            $mail->setFrom(config('mail.fromuser'), config('mail.fromusername'));

            $mail->addAddress($email); 

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $title;
            $mail->Body    = $html;
            // var_dump($mail);
            $mail->send();

            echo 'Message has been sent';
        } catch (Exception $e) {

            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }
}