<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-12 15:43:47 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-12 16:55:17
 */
include './common.php';

$data = $_POST;


if(empty($data['username']) || empty($data['content'])){

	notice('./index.php',"用户名或者内容为空",false,3);
}


$data['ctime'] = time();

// $oldData = file_get_contents('./db/data.txt');

// $oldArr = json_decode($oldData,true);
$oldArr = getData();

// $oldArr[] = $data;

array_unshift($oldArr,$data);

// $str = json_encode($oldArr);

// file_put_contents('./db/data.txt',$str);
// 
putData($oldArr);

// header("Location:./index.php");
// 
notice('./index.php',"发布成功",true,1);

?>
