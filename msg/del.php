<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-12 16:29:38 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-19 11:35:26
 */
include 'common.php';

if(!is_login()){
	notice("./login.php",'你还没有登录，请先登录',false);
}

if(isset($_GET['key'])){

	$key = $_GET['key'];

	$oldData = getData();
	// array_splice($oldData, $key ,1);
	unset($oldData[$key]);

	putData($oldData);

	notice("./index.php",'删除成功');

}else{

	notice("./index.php",'参数有误',false);
}


