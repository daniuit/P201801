<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-12 15:57:26 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-19 11:32:45
 */
session_start();

define('CSS', './tempelte/css');

define('JS', './tempelte/js');


function notice($url='./index.php',$info="发布成功",$is=true,$time=3)
{
	include './tempelte/notice.html';
	exit;
}

function getData($url = './db/data.txt')
{
	$oldData = file_get_contents($url);

	$oldArr = json_decode($oldData,true);

	return $oldArr;
}

function putData($data,$url = './db/data.txt')
{
	$str = json_encode($data);

	file_put_contents($url,$str);
}

/*
 * [Ftime 格式化时间]
 * @param [type] $time [时间戳]
 */
function Ftime($time=null)
{
	if($time){

		$diff = time()-$time;

		if($diff<60 && $diff>=0){
			return "刚刚";
		}elseif($diff<3600){
			return floor($diff/60)."分钟之前";
		}elseif($diff<86400){
			return floor($diff/3600)."小时之前";
		}elseif($diff<86400*7){
			return floor($diff/86400)."天之前";
		}else{
			return date("Y-m-d H:i:s",$time);
		}
	}else{
		return date("Y-m-d H:i:s");
	}
}

function is_login()
{
	if(isset($_SESSION['username'])){
		return true;
	}else{
		return false;
	}
}