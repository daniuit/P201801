<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-12 16:43:33 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-12 16:50:53
 */

include 'common.php';

$data = $_POST;

$key = $_GET['key'];

if(empty($data['username']) || empty($data['content'])){

	notice('./edit.php?key='.$key,"用户名或者内容为空",false,3);
}

$data['ctime'] = time();

$oldData = getData();

$oldData[$key] = $data;

putData($oldData);

notice('./index.php',"修改成功",true,3);

