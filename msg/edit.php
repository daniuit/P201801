<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-12 16:37:04 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-19 11:33:28
 */
include 'common.php';

if(!is_login()){
	notice("./login.php",'你还没有登录，请先登录',false);
}

if(isset($_GET['key'])){

	$key = $_GET['key'];

	$oldData = getData();

	$row = $oldData[$key];

	include './tempelte/edit.html';
}else{

	notice("./index.php",'参数有误',false);
}

