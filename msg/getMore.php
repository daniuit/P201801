<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-20 09:35:06 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-20 09:42:19
 */
include './common.php';

$p = $_POST['p'];

$oldArr = getData();

$cn = count($oldArr);

$num = 3;

$oldArr = array_slice($oldArr,($p-1)*$num,$num);

if(empty($oldArr)){

	echo json_encode(['error'=>1,'info'=>'数据已经莫了']);
	exit;
}else{

	$tempArr= [];
	foreach ($oldArr as $k => $row) {
		$row['ctime'] = Ftime($row['ctime']);
		$tempArr[$k] = $row;
	}
	echo json_encode($tempArr);
}
