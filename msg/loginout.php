<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-19 11:27:20 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-19 11:31:20
 */
include './common.php';

session_unset();
session_destroy();

notice('./index.php',"退出成功");