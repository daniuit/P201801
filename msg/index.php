<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-12 15:38:35 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-20 10:32:20
 */
include './common.php';

$oldArr = getData();

$cn = count($oldArr);

$num = 3;

$page = ceil($cn/$num);

$p = isset($_GET['p']) ? $_GET['p'] : 1;

$oldArr = array_slice($oldArr,($p-1)*$num,$num);

if($p<=2){
	$start = 1;
	$end = 5;
}else{
	$start = $p-2;
	$end = $p+2;
}

if($p>=($page-1)){
	$end = $page;
	$start = $page-4;
}


// var_dump($cn);
// var_dump($cn);
// var_dump($num);
// var_dump($page);
// var_dump($p);

// var_dump($_SESSION);

include './tempelte/index.html';