<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-11 14:19:35 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-11 15:47:33
 */
// 项目入口
// 引公共文件
include './common.php';
//读取数据库文件数据 
$data = file_get_contents("./db/data.txt");
// 把字符串转成数组
$data = json_decode($data,true);
// 引首页的模板
include './templete/index.html';