<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-11 15:11:20 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-11 15:49:46
 */
// 增加留言
// 接收表单post过来的数据 
$data = $_POST;
// 增加留言时间
$data['ctime'] = time();
//把数据库的老数据读出来
$oldData = file_get_contents("./db/data.txt");
//把字符串转成数组
$oldArr = json_decode($oldData,true);
// 往老数据前头追加一个新数据
// $oldArr[] = $data;
array_unshift($oldArr, $data);
//把数组转成字符 串
$str = json_encode($oldArr);
// 写入到数据库文件
file_put_contents('./db/data.txt',$str);

// php跳转首页
header("Location:./index.php");

