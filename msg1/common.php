<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-11 14:32:01 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-11 15:35:29
 */
define('JS', './templete/js');

define('CSS', './templete/css');
/*
 * [Ftime 格式化时间]
 * @param [type] $time [时间戳]
 */
function Ftime($time=null)
{
	if($time){

		$diff = time()-$time;

		if($diff<60 && $diff>=0){
			return "刚刚";
		}elseif($diff<3600){
			return floor($diff/60)."分钟之前";
		}elseif($diff<86400){
			return floor($diff/3600)."小时之前";
		}elseif($diff<86400*7){
			return floor($diff/86400)."天之前";
		}else{
			return date("Y-m-d H:i:s",$time);
		}
	}else{
		return date("Y-m-d H:i:s");
	}
}

