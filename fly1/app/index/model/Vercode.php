<?php

namespace app\index\model;

use think\Model;
use think\Db;

class Vercode extends Model
{
    
    public function getRandone()
    {
         $question = Db::query('select * from fly_vercode order by rand() limit 1');

         $question = current($question);

         session('answer',$question['answer']);

         return $question['question'];
    }
}