<?php

namespace app\index\controller;

use think\Controller;
use app\index\model\Vercode;
use app\index\model\User;
use think\Loader;
use think\Db;

class Register extends Controller
{
    
    public function index()
    {

        $quesion = (new Vercode())->getRandone();
        //分配变量
        $this->assign('quesion',$quesion);
        // 引视图
        return $this->fetch();
    }


    public function checkData()
    {
        $data = input('post.');

        $validate = Loader::validate('Reg');

        if(!$validate->check($data)){
            $this->error($validate->getError());
        }

        $data['password'] = md5($data['password']);

        $data['ctime']  = time();

        // $res = Db::table('fly_user')->insert($data);
        
        // $user = new User();

        // $user->data($data);

        // $res = $user->save();
    
        $res = (new User())->data($data)->allowField(true)->save();

        if($res){
             $this->success('注册成功');
        }else{
             $this->error('注册失败');
        }
    
    }



}