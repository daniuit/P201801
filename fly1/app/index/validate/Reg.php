<?php

namespace app\index\validate;

use think\Validate;

class Reg extends Validate
{
    
    protected $rule = [
        'agreement'  =>  'require',
        'password' =>'require|length:6,10|alphaNum|confirm:repassword',
        'vercode'  => 'require|checkVerCode',
        'nickname' =>'require|alpha|length:6,8|unique:user',
        'phone' => 'require|regex:1\d{10}|unique:user'
    ];

    protected $message  =   [
        'agreement.require' => '服务协议必须同意',  
        'password.require'=> '密码必须真',
        'password.alphaNum'=> '密码必须由数字与字母组成',
        'password.length'=> '密码必须由6-10',
    ];

    public function checkVerCode($value)
    {
        if($value==session('answer')){
            return true;
        }else{
            return false;
        }
    }
}

