/*
* @Author: zhibinm
* @Date:   2018-04-25 08:04:40
* @Last Modified by:   Zhibinm
* @Last Modified time: 2018-04-25 09:20:09
*/
-- 用户表
create table user(
    uid int(7) primary key AUTO_INCREMENT COMMENT "主键",
    nickname char(20) not null COMMENT "昵称",
    password char(32) not null COMMENT "密码",
    email char(50) unique not null COMMENT "邮箱",
    face char(100) not null default '' COMMENT "头像",
    kiss int(7) not null default 0 COMMENT "飞吻",
    level enum('0','1','2','3','4') default '0' COMMENT "等级", 
    openid char(32) not null default '' COMMENT "QQ绑定的id",
    weibo_id char(32) not null default '' COMMENT "微博绑定的id",
    sign char(200) not null default '' COMMENT "签名",
    sex enum('男','女','保密') default "男",
    phone char(11) unique not null default '' COMMENT "手机",
    city char(10) not null default '' COMMENT "城市",
    login_ip char(15) COMMENT "ip",
    status enum('0','1') default "0" COMMENT "0正常，1禁止",
    is_admin enum('0','1') default "0" COMMENT "是否管理员",
    verify enum('0','1') default "0" COMMENT "0手机未验证，1已验证",
    ctime int(10) not null default 0 COMMENT "创建时间",
    ltime int(10) not null default 0 COMMENT "登录时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='用户表';
-- 提问表
create table question(
    qid int(7) primary key AUTO_INCREMENT COMMENT "主键",
    title char(100) not null COMMENT "标题",
    content text not null COMMENT "内容",
    cid int(5) not null COMMENT "分类id",
    uid int(5) not null COMMENT "用户id",
    kiss int(7) not null default 0 COMMENT "飞吻",
    replay int(7) not null default 0 COMMENT "回复数",
    view int(7) not null default 0 COMMENT "浏览数",
    is_del enum('0','1') default "0" COMMENT "0正常，1禁用",
    status enum('0','1') default "0" COMMENT "0未结，1已结",
    is_jing enum('0','1') default "0" COMMENT "0未结，1精华",
    is_top enum('0','1') default "0" COMMENT "0未结，1置顶",
    ctime int(10) not null default 0 COMMENT "创建时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='提问表';

-- 回复表

create table replay(
    rid int(7) primary key AUTO_INCREMENT COMMENT "主键",
    uid int(5) not null COMMENT "用户id",
    qid int(5) not null COMMENT "问题id",
    content text not null COMMENT "内容",
    status enum('0','1') default "0" COMMENT "0没被采纳，1被采纳",
    zan_num int(7) not null default 0 COMMENT "点赞数",
    ctime int(10) not null default 0 COMMENT "创建时间"  
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='回复表';

-- 分类表

create table category(
    cid int(7) primary key AUTO_INCREMENT COMMENT "主键",
    cname char(20) not null default '' COMMENT "分类",
    list_order int(5) not null default 0 COMMENT "排序",
    status enum('0','1') default "0" COMMENT "0普通会员可以进行发布，1管理员进行发布",
    ctime int(10) not null default 0 COMMENT "创建时间",  
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='分类表';

-- 签到表

create table sign(
    sid int(7) primary key AUTO_INCREMENT COMMENT "主键",
    uid int(5) not null COMMENT "用户id",
    stime int(10) not null default 0 COMMENT "签到时间"
    num int(7) not null default 0 COMMENT "连续签到数量" 
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='签到表';

-- 友情链接表
create table link(
    lid int(7) primary key AUTO_INCREMENT COMMENT "主键",
    lname char(20) not null default '' COMMENT "链接名",
    url char(20) not null default '' COMMENT "链接地址",
    list_order int(5) not null default 0 COMMENT "排序",
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='友情表';


-- 案例表

create table demo(
    did int(7) primary key AUTO_INCREMENT COMMENT "主键",
    thumb char(100) not null COMMENT "缩略图",
    title char(100) not null COMMENT "标题",
    content text not null COMMENT "简介",
    uid int(5) not null COMMENT "用户id",
    zan_num int(7) not null default 0 COMMENT "回复数",
    ctime int(10) not null default 0 COMMENT "创建时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='提问表';

-- 案例点赞表

create table demo_zan(
    did int(5) not null COMMENT "案例id",
    uid int(5) not null COMMENT "用户id",
    ctime int(10) not null default 0 COMMENT "创建时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='安全点赞表';


-- 广告表

create table ad(
    aid int(7) primary key AUTO_INCREMENT COMMENT "主键",
    aname char(20) not null default '' COMMENT "广告名",
    url char(20) not null default '' COMMENT "链接地址",
    list_order int(5) not null default 0 COMMENT "排序",
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='广告表';

-- 收藏表

create table collect(
    qid int(5) not null COMMENT "问题id",
    uid int(5) not null COMMENT "用户id",
    ctime int(10) not null default 0 COMMENT "创建时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='收藏表';


-- 点赞表
create table replay_zan(
    rid int(5) not null COMMENT "回复id",
    uid int(5) not null COMMENT "用户id",
    ctime int(10) not null default 0 COMMENT "创建时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='点赞表';

消息表

create table msg(
    mid int(5) not null COMMENT "回复id",
    duid int(5) not null COMMENT "目标用户id",
    suid int(5) not null COMMENT "源用户id",
    type enum('replay','zan','collect') COMMENT "类型",
    qid int(5) not null COMMENT "问题id",
    rid int(5) not null COMMENT "回复id",
    ctime int(10) not null default 0 COMMENT "创建时间"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='消息表';

验证表

create table vercode(
    vid int(5) not null COMMENT "回复id",
    question char(100) not null '' COMMENT "问题",
    answer char(100) not null default '' COMMENT "答案"
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='验证表';

