<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-11 09:59:31 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-11 10:47:01
 */
// $str = 'ERDT$ERT';
// $str = "$DFGDFG";
// $str = <<<xbs

// xbs;


// $data = $_GET['str'];

// var_dump($data);
// $res = addslashes($data);
// var_dump($res);

// echo 'sdfsdfsdfsdf';
// 
// $str1 = "456|4564|456|45364|5645";
// $str2 = "45,4564,456,45364,645";
 
// var_dump(explode(',', $str2));
// 
$arr = [456,456,45,645,6,4];

// echo implode('|',$arr);


$str ="<p>学并思</p>";

$str = <<<xbs
<div id="top">
		<div class="mid">
			<div class="top_left">
				<a href="">小米商城</a>|
				<a href="">MIUI</a>|
				<a href="">IoT</a>|
				<a href="">云服务</a>|
				<a href="">水滴</a>|
				<a href="">金融</a>|
				<a href="">有品</a>|
				<a href="">Select Region</a>
			</div>
			<div class="top_right">
				<a href="">登录</a>|
				<a href="">注册</a>|
				<a href="">消息通知</a>
				<a class="car" href=""><span>&#xe60a;</span>购物车(0)
					<div class="car_show">67</div>
				</a>
			</div>
		</div>
	</div>
xbs;

$str = <<<xbs
	<div class="clearfix" id="p_content"><p style="text-indent: 2em;">
	3月28日，习近平总书记在博鳌亚洲论坛发表主旨演讲。场内，掌声雷动、喝彩满堂，场外，“习式演讲”广受世界媒体追捧。习近平到底讲了哪些重磅内容，将对世界未来产生什么影响？人民日报全媒体平台（中央厨房）小厨现在帮你梳理消化——</p><p style="text-indent: 2em;">
	<strong>“习式四观”为国际政治新秩序注入中国智慧</strong></p><p style="text-indent: 2em;">
	面对风云变幻的国际和地区形势，习近平倡导“共同营造对亚洲、对世界都更为有利的地区秩序，通过迈向亚洲命运共同体，推动建设人类命运共同体。”</p><p style="text-indent: 2em;">
	在“迈向命运共同体”的主题引领下，习近平在演讲中提出 “四个坚持”，为新型国际关系和国际政治新秩序注入“中国智慧”。小厨帮你拎出几条“干货”。</p><p style="text-indent: 2em;">
	<strong>政治观：迈向命运共同体，必须坚持各国相互尊重、平等相待。</strong></p><p style="text-indent: 2em;">
	<strong>理念：</strong></p><p style="text-indent: 2em;">
	1、 各国体量有大小、国力有强弱、发展有先后，但都是国际社会平等的一员，都有平等参与地区和国际事务的权利。</p><p style="text-indent: 2em;">
	2、 涉及大家的事情要由各国共同商量来办。</p><p style="text-indent: 2em;">
	3、 作为大国，意味着对地区和世界和平与发展的更大责任，而不是对地区和国际事务的更大垄断。</p><p style="text-indent: 2em;">
	<strong>主张：</strong></p><p style="text-indent: 2em;">
	1、要尊重各国自主选择的社会制度和发展道路，尊重彼此核心利益和重大关切，客观理性看待别国发展壮大和政策理念，努力求同存异、聚同化异。</p><p style="text-indent: 2em;">
	2、要共同维护亚洲来之不易的和平稳定局面和良好发展势头，反对干涉别国内政，反对为一己之私搞乱地区形势。</p><p style="text-indent: 2em;">
	<strong>合作观：迈向命运共同体，必须坚持合作共赢、共同发展。</strong></p><p style="text-indent: 2em;">
	<strong>理念：</strong></p><p style="text-indent: 2em;">
	1、东南亚朋友讲“水涨荷花高”，非洲朋友讲“独行快，众行远”，欧洲朋友讲“一棵树挡不住寒风”，中国人讲“大河有水小河满，小河有水大河满”。这些说的都是一个道理，只有合作共赢才能办大事、办好事、办长久之事。</p><p style="text-indent: 2em;">
	2、要摒弃零和游戏、你输我赢的旧思维，树立双赢、共赢的新理念，在追求自身利益时兼顾他方利益，在寻求自身发展时促进共同发展。</p><p style="text-indent: 2em;">
	3、合作共赢的理念不仅适用于经济领域，也适用于政治、安全、文化等广泛领域；不仅适用于地区国家之间，也适用于同域外国家开展合作。</p><p style="text-indent: 2em;">
	<strong>主张：</strong></p><p style="text-indent: 2em;">
	要加强宏观经济政策协调，防范不同经济体经济政策变动可能带来的负面外溢效应，积极推动全球经济治理变革，维护开放型世界经济体制，共同应对世界经济中的风险和挑战。</p><p style="text-indent: 2em;">
	<strong>安全观：迈向命运共同体，必须坚持实现共同、综合、合作、可持续的安全。</strong></p><p style="text-indent: 2em;">
	<strong>理念：</strong></p><p style="text-indent: 2em;">
	1、当今世界，没有一个国家能实现脱离世界安全的自身安全，也没有建立在其他国家不安全基础上的安全。我们要摒弃冷战思维，创新安全理念，努力走出一条共建、共享、共赢的亚洲安全之路。</p><p style="text-indent: 2em;">
	2、各国都有平等参与地区安全事务的权利，也都有维护地区安全的责任，每一个国家的合理安全关切都应该得到尊重和保障。</p><p style="text-indent: 2em;">
	<strong>主张：</strong></p><p style="text-indent: 2em;">
	1、要通盘考虑亚洲安全问题的历史经纬和现实状况，多管齐下、综合施策，协调推进地区安全治理，统筹维护传统和非传统领域安全。</p><p style="text-indent: 2em;">
	2、要通过对话合作促进各国和本地区安全，以合作谋和平、以合作促安全，坚持以和平方式解决争端，反对动辄使用武力或以武力相威胁。</p><p style="text-indent: 2em;">
	3、要坚持发展和安全并重，以可持续发展促进可持续安全。亚洲国家要加强同其他地区国家和有关组织合作，欢迎各方为亚洲发展和安全发挥积极和建设性作用。</p><p style="text-indent: 2em;">
	<strong>文明观：迈向命运共同体，必须坚持不同文明兼容并蓄、交流互鉴。</strong></p><p style="text-indent: 2em;">
	<strong>理念：</strong></p><p style="text-indent: 2em;">
	1、今天的亚洲，多样性的特点仍十分突出，不同文明、不同民族、不同宗教汇聚交融，共同组成多彩多姿的亚洲大家庭。</p><p style="text-indent: 2em;">
	2、中国古代思想家孟子说过：“夫物之不齐，物之情也。”不同文明没有优劣之分，只有特色之别。</p><p style="text-indent: 2em;">
	<strong>主张：</strong></p><p style="text-indent: 2em;">
	要促进不同文明不同发展模式交流对话，在竞争比较中取长补短，在交流互鉴中共同发展，让文明交流互鉴成为增进各国人民友谊的桥梁、推动人类社会进步的动力、维护世界和平的纽带。</p><p style="text-indent: 2em; text-align: center;">
	<a href="/n/2015/0328/c1001-26764563-2.html"><img width="271" height="86" alt="" src="/NMediaFile/2015/0328/MAIN201503281350000015583115480.png"></a></p><div class="zdfy clearfix"><a href="/n/2015/0328/c1001-26764563.html">【1】</a><a href="/n/2015/0328/c1001-26764563-2.html">【2】</a></div><center><table width="40%" align="center" border="0"><tbody><tr><td width="50%" align="center"><a href="/n/2015/0328/c1001-26764563-2.html">下一页</a></td></tr></tbody></table></center></div>
xbs;

// echo strip_tags($str);

// echo $str;
// $res = htmlspecialchars($str);

// echo htmlspecialchars_decode($res);
// 

// $name= $_GET['str'];

// $img = "4356456.jpg";

// var_dump(rtrim($img,'.jpg'));

// var_dump(trim($name));

// if('xuebingsi' == ' xuebingsi')

// trim();
// ltrim()
// rtrim();
// 
$password = 'xbs'.'123456'.'xbs';

// var_dump(md5($password));  //32位字符 串

// echo  sha1($password);
// 
// 
// $str = "hello world";

// echo str_replace('H', 'aa', $str);
// 

// echo str_repeat('a',100);
// 
$str = "学并思";
// echo strslen($str);
// echo stripos($str,'j');

$str = "sdfklJHkiAHSLOHL";

$str = "我们一起去吃饭";

// echo ucfirst($str);

echo ucwords($str);

// echo substr($str,2,4);

// echo strtolower($str);

// echo strtoupper($str);
// 
// 