<?php

include '../functions.php';
include './Model.class.php';

// $data = config('db_host');

// var_dump($data);

// $model = new Model();

// $res = $model->query('select * from sturtr');

// if(!$res){
//     $data = $model->getErrorInfo();

//     var_dump($data);
// }

// var_dump($res);
// // 单例模式

// $model = new Model();

// $res = $model->exec("update stu set sname='小是' where sid=90");

// if(!$res){
//     $data = $model->getErrorInfo();

//     var_dump($data);
// }

// var_dump($res);

// $user = new Model('user');

// $data = ['sname'=>'小明','sex'=>'女'];

// $user->where(['sid'=>6])->update($data);

// $data = ['sname'=>'小明','sex'=>'女'];

// $user->add($data);

// $user->where(['sname'=>'小明'])->del();


$user = new Model('stu');

$data = ['sname'=>'小花小花小花小花','sex'=>'男','cid'=>4];

$user->where(['sex'=>'男'])->update($data);


// $user->where(['sex'=>'男'])->del();

// $data = ['sname'=>'小明','age'=>30,'sex'=>'女','cid'=>4];

// $user->add($data);

// 链式操作

// $res = $user->where(['sex'=>'男'])->select();

// var_dump($res);
