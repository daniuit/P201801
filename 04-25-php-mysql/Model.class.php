<?php

/**
* mysql数据库操作模型
*/
class Model
{
    public static $db; //放pdo数据库对象
    public $errorInfo; //放错误信息
    public $table; //放表名
    public $where = '';//放where条件
    public $limit = '';//放limit数量 
    /**
     * [__construct 构造函数 实例pdo对象]
     * @param string $table [description]
     */
    public function __construct($table='')
    {
        if($table){

            $this->table = $table;
        }

        if(!self::$db){
            self::$db = new PDO(config('db_type').":host=".config('db_host').";dbname=".config('db_name').";charset=".config('db_charset'),config('db_user'),config('db_pass'));
        }
    }
    /**
     * [query 执行原生sql查询 ]
     * @param  [type] $sql [description]
     * @return [type]      [description]
     */
    public function query($sql)
    {
        $res = self::$db->query($sql);

        if($res){

            return $res->fetchAll(PDO::FETCH_ASSOC);

        }else{
            $this->errorInfo = self::$db->errorInfo();
            return false;
        }
    }
    /**
     * [exec 执行原生sql 增删改]
     * @param  [type] $sql [description]
     * @return [type]      [description]
     */
    public function exec($sql)
    {
        $res = self::$db->exec($sql);

        if($res===false){
            echo 999;
            $this->errorInfo = self::$db->errorInfo();
            return false;

        }else{
            echo 8888;
            return true;
        }
    }
    /**
     * [getErrorInfo 获取错误信息]
     * @return [type] [description]
     */
    public function getErrorInfo()
    {
        return $this->errorInfo;
    }

    /**
     * [select 查询]
     * @return [type] [description]
     */
    public function select()
    {
        $sql = "select * from ".$this->table.$this->where.$this->limit;

        return $this->query($sql);
        
    }
    /**
     * [where where条件]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public function where($where)
    {

        $this->where = " where";

        // 拼接字符
        foreach ($where as $k => $v) {
            if(is_array($v)){
                 $this->where .=" ".$k.$v[0]."'".$v[1]."' and";
            }else{
                $this->where .=" ".$k."='".$v."' and"; 
            }
        }

        $this->where = rtrim($this->where,'and');

        return $this; 
    }
    /**
     * [limit limt条件]
     * @param  [type] $num [description]
     * @return [type]      [description]
     */
    public function limit($num)
    {
        $this->limit = " limit ".$num;
        return $this;
    }
    /**
     * [add 增加]
     * @param [type] $data [description]
     */
    public function add($data)
    {

        $key = array_keys($data);

        $skey = implode(',',$key);

        $svalue  = implode("','",$data);

        $sql = " insert into ".$this->table."(".$skey.") value('".$svalue."')";

        return $this->exec($sql);

    }
    /**
     * [del 删除]
     * @param  string $id [description]
     * @return [type]     [description]
     */
    public function del($id='')
    {
        if($id){

            $key = $this->getPrimaryKey();

            $sql = "delete from ".$this->table ." where ".$key."=".$id;
            return $this->exec($sql);

        }else{

            $sql = "delete from ".$this->table .$this->where;

            return $this->exec($sql);
        }
    }
    /**
     * [getPrimaryKey 获取主键]
     * @return [type] [description]
     */
    public function getPrimaryKey()
    {
        $sql = "desc ".$this->table;

        $data = $this->query($sql);

        foreach ($data as  $row) {
            if($row['Key']=='PRI'){
                return $row['Field'];
            }
        }

        return false;
    }
    /**
     * [update 更新]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function update($data)
    {

        $key = $this->getPrimaryKey();


        $setStr = '';
        // 拼接字符
        foreach ($data as $k => $v) {
            if($k<>$key){
                $setStr .= $k ."='".$v."',";
            }
        }

        $setStr = rtrim($setStr,',');

        if(array_key_exists($key,$data)){

            $sql ="update ".$this->table." set ".$setStr." where ".$key."=".$data[$key];

           return $this->exec($sql);

        }else{

           $sql ="update ".$this->table." set ".$setStr.$this->where;

           return $this->exec($sql);     

        }
    }
}