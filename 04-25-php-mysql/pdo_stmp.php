<?php

// 链接数据 库
$db = new PDO("mysql:host=127.0.0.1;dbname=school;charset=utf8","root","root");

// 准备要执行的语句，用占位符代替想要传的数据 
$sql = "insert into stu (sname,age,sex) values(:sname,:age,:sex)";

//发给mysqli 进行准备，准备成功返回一个准备对象

$stmp = $db->prepare($sql);

$stmp->bindParam(':sname', $sname, PDO::PARAM_STR,20);
$stmp->bindParam(':age', $age, PDO::PARAM_INT, 12);
$stmp->bindParam(':sex', $sex, PDO::PARAM_STR, 1);


for ($i=0; $i <200 ; $i++) { 
    $sname ="小花".$i;

    $age = 90;

    $sex = "男";

    $stmp->execute();
}

