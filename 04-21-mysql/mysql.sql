/*
* @Author: zhibinm
* @Date:   2018-04-21 09:14:46
* @Last Modified by:   Zhibinm
* @Last Modified time: 2018-04-21 12:05:47
*/
 
 -- php客户端    mysql服务端

 -- 本地浏览器客户端   php服务端

 -- 本地cmd命令行客户端   mysql服务端

 -- mysql -uroot -p

 -- mysql 里面建设多个库

 -- 四要素

 -- 库 （某个项目相关数据的集合）（表的集合）

 -- 表 （存储某一业务的数据集合）

 -- 行  (一条数据）;

 -- 列 (类似表格的单元格，，，，，，，字段)

-- 查看数据 库
 show databases;
 SHOW DATABASES;
 -- 创建数据库
 create database school;
 -- 创建数据库 指定编码
 create database xbs_bbs default character set utf8;
 -- 删除库
 drop database school;
 -- 使用某个库	
 use xbs_bbs;
 -- 查看表
 show tables;

 -- ()  [] {}

-- 创建表 修饰
 create table stu(
 	id int(7) primary key AUTO_INCREMENT COMMENT "主键",
 	name char(20) not null COMMENT "学生名",
 	num int(6) zerofill COMMENT "号码",
	age TINYINT unsigned default 20 COMMENT "年龄",
	sex set('男','女','保密') default "男",
	addr char(50) default "",
	phone char(11) default '广东',
	birthday date default '2018-09-14',
	price DECIMAL(7,3)
)ENGINE=MyISAM AUTO_INCREMENT=1 COMMENT='学生表';
-- 查看表结构 
desc stu;


-- stu

-- id	int(7)

-- name char(20)

-- age	TINYINT

-- sex	set('男','女','保密')

-- addr char(50)

-- phone char(11)	

-- birthday date	

-- price DECIMAL(7,3)
CREATE TABLE `cmf_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '用户类型;1:admin;2:会员',
  `sex` tinyint(2) NOT NULL DEFAULT '0' COMMENT '性别;0:保密,1:男,2:女',
  `birthday` int(11) NOT NULL DEFAULT '0' COMMENT '生日',
  `last_login_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '用户积分',
  `coin` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '金币',
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '余额',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '注册时间',
  `user_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '用户状态;0:禁用,1:正常,2:未验证',
  `user_login` varchar(60)  NOT NULL DEFAULT '' COMMENT '用户名',
  `user_pass` varchar(64) NOT NULL DEFAULT '' COMMENT '登录密码;cmf_password加密',
  `user_nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `user_email` varchar(100) NOT NULL DEFAULT '' COMMENT '用户登录邮箱',
  `user_url` varchar(100) NOT NULL DEFAULT '' COMMENT '用户个人网址',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '用户头像',
  `signature` varchar(255) NOT NULL DEFAULT '' COMMENT '个性签名',
  `last_login_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '' COMMENT '激活码',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '用户手机号',
  `more` text COMMENT '扩展属性',
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';


CREATE TABLE `cmf_auth_rule` (
  `id` int(10) unsigned NOT NULL  PRIMARY KEY  AUTO_INCREMENT COMMENT '规则id,自增主键',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `app` varchar(15) NOT NULL COMMENT '规则所属module',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '权限规则分类，请加应用前缀,如admin_',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `param` varchar(100) NOT NULL DEFAULT '' COMMENT '额外url参数',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '规则描述',
  `condition` varchar(200) NOT NULL DEFAULT '' COMMENT '规则附加条件'
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8mb4 COMMENT='权限规则表';


-- 设置编码  
set names gbk;

-- 增 删 查 改

-- 查表数据

select * from stu;

insert into stu (name,age,addr) values('小明',18,"广东");

insert into stu (name,age,addr) values('小明',18,"广东"),('小红',20,'湖南');

delete from stu where name='小明';

delete from stu where id<3;

drop table stu;

drop database school;

update stu set age =30 where sex="男";

update stu set age =50,sex="女" where id=4;