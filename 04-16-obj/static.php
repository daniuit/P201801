<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-16 17:10:08 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-16 17:28:19
 */
/**
* 静态属性，多个对象中进行共享的存储容器
*/
/**
* 
*/
class B
{
	public static $cn = 100;
}
class A extends B
{
	public $num=0;

	public function run()
	{
		$this->num++;

		parent::$cn++;

		echo parent::$cn;
	}
	public static function test()
	{
		echo "test";
	}
}

// A::test();

// echo A::$cn;

// $obj = new A();

// $obj->run();

// $obj = new A();

// $obj->run();
// 

/**
* 
*/
class cc
{	
	function A(argument)
	{
		$this->b;
	}
	function b(argument)
	{
		# code...
	}
	function c(argument)
	{
		# code...
	}
	function d(argument)
	{
		# code...
	}
	function E(argument)
	{
		# code...
	}
	public static function bb($value='')
	{
		# code...
	}
}


cc::bb();

$obj = new cc();
$obj->a();

