<?php

/**
* 汽车类
*/
class Car 
{
	public $name ="奥迪";
	// 两个会自动执行的函数 
	public $color;
	/**
	 * [__construct 当类被实例化时候会自己函数 ]
	 */
	public function __construct($color="黑色")
	{
		$this->color = $color;
	}

	public function run()
	{
		echo $this->name."可以跑";
	}
	public function ziaren()
	{
		echo $this->color.$this->name."我可以裁人";
	}
	public function paoniu()
	{
		echo "我可以泡妞";
	}
	/**
	 * [__destruct 对象销毁自动执行函数]
	 */
	public function __destruct()
	{
		echo "我自动消失了";
	}
}

