<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-10 08:14:40 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-10 09:07:42
 */

function get_max($num1,$num2){

	// if($num1>$num2){
	// 	return $num1;
	// }else{
	// 	return $num2;
	// }
	
	return $num1>$num2 ? $num1 : $num2;
}


// $ddd = get_max(1256,665);

// echo $ddd;

function create_table($row,$col){

	echo "<table border='1'>";
		for ($i=0; $i < $row; $i++) { 
			echo "<tr>";
				for ($k=0; $k <$col ; $k++) { 
					echo "<td>";
					echo "***";
					echo "</td>";
				}
			echo "</tr>";
		}
	echo "</table>";

	return true;
}

// $res = create_table(10,2);
// $res = create_table(5,6);
// $res = create_table(10,6);
// var_dump($res);
// 


function create_date_table($day=31,$w=3){

	echo "<table border='1'>";
	echo "<tr><th>星期日</th><th>星期一</th><th>星期二</th><th>星期三</th><th>星期四</th><th>星期五</th><th>星期六</th></tr>";
	$a = 1;
	$end = false;
	for ($i=0; $i < 6; $i++) { 
			echo "<tr>";
				for ($k=0; $k <7 ; $k++) { 
					// 判断大于最后一天是否要补空格
					if($a>$day){
						// 大于最后一天在第一格，整行不要
						if($k==0){
							break;
						}
						//补空格
						$end = true;
						echo "<td>";
						echo "</td>";
						continue;
					}
					//判断星期天或者星期六
					if($k==0 || $k==6){
						echo "<td style='color:red'>";
					}else{
						echo "<td>";
					}
					//判断1号在第几个格出现，大于1号的递增
					if($a>1){
						echo $a;
						$a++;
					}else if($k==$w){
						echo $a;
						$a++;
					}
					echo "</td>";
				}
			echo "</tr>";

			// 判断在于最后一天，后面行数就不需要走了
			if($end){
				break;
			}
	}
	echo "</table>";
}

create_date_table(28,1);