<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-10 10:11:01 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-10 10:22:23
 */

// echo abs(-90);
// echo ceil(90.676);
// echo floor(121.90);
// echo max(546,45,645,6,45,62,423,4234);
// echo min(456,45,64,56,45,645,6);
// echo round(456.45645);
// echo rand(1,47);
// echo mt_rand(5,10);//更好的随机数
// echo pow(1024,4);//指数

封闭一个函数 ，传入字节数，根据字节数转成可以理解的单位

// // 字节是一种存储单位
// 56565651  b kb mb gb tb   1024
// echo round(456.45645,2);