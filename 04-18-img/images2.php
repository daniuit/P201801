<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-18 17:16:53 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-18 17:24:58
 */
$bg = imageCreateFromjpeg('./bg.jpg');

$bw = imagesx($bg);

$by = imagesy($bg);

$small = imageCreateTrueColor($bw*0.1 ,$by*0.1);

$mid = imageCreateTrueColor($bw*0.3 ,$by*0.3);

$big = imageCreateTrueColor($bw*0.5 ,$by*0.5);

imagecopyresized ($small ,$bg , 0 , 0 , 0 , 0 , $bw*0.1 , $by*0.1 ,$bw  , $by );

imagecopyresized ($mid ,$bg , 0 , 0 , 0 , 0 , $bw*0.3 , $by*0.3 ,$bw  , $by );

imagecopyresized ($big ,$bg , 0 , 0 , 0 , 0 , $bw*0.5 , $by*0.5,$bw  , $by );

imagepng($small,'./file/'.uniqid().'small.png');
imagepng($mid,'./file/'.uniqid().'mid.png');
imagepng($big,'./file/'.uniqid().'big.png');


