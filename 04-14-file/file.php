<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-16 14:55:13 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-16 15:02:01
 */

/**
 * [delDir 递归删除目录]
 * @param  [type] $dir [目录名]
 * @return [type]      [无返回值]
 */
function delDir($dir)
{
	$files = glob($dir.'/*');

	foreach ($files as  $file) {
		if(is_dir($file)){
			delDir($file);
		}else{
			unlink($file);
		}
	}

	rmdir($dir);
}

delDir('./xbs');

