<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-04-14 11:27:00 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-04-14 11:34:49
 */

function upload($file)
{
	//把文件名转成数据
	$suffix = explode(".", $file['name']);
	//取数组里面最后一项
	$suffix = end($suffix);

	// 设置允许 的类型
	$allowType = ['jpg','png','gif','txt'];
	//判断当前 上传的文件是否符合对应的类型
	if(!in_array($suffix,$allowType)){
		exit("文件类型不允许，允许的类型".implode(',',$allowType));
	}
	//判断文件大小
	if($file['size']>100000){
		exit("文件过大最大是100字节");
	}
	//根据微秒来生成一个唯一文件id
	$name =uniqid().".".$suffix;

	// 目录存在不创建，不存在创建目录
	is_dir("./Upload") || mkdir('./Upload');

	// 把文件的移动到想要的目录进行保存
	move_uploaded_file($file['tmp_name'],"./Upload/".$name);
}

var_dump($_FILES);
// upload();
// 
foreach ($_FILES as  $row) {
	upload($row);
}
